CC     :=  gcc
CFLAGS := -Wall -Wextra -pedantic -std=c99 -g
LIBS   := -lpthread
RM     := rm -f

.PHONY: all clean

TARGET := bumper_cars
SRC := $(wildcard *.c)
OBJ := $(SRC:.c=.o)

all: $(OBJS) $(TARGET)

#
# link the .o files into the target executable
#
$(TARGET): $(OBJS)
    $(CC) $^ -o $@ $(LIBS)

#
# compile the .c file into .o files using the compiler flags
#
%.o: %.c sleeper.h
    $(CC) $(CFLAGS) -c $< -o $@ -I.


clean:
    $(RM) *.o
    $(RM) bumper_cars
