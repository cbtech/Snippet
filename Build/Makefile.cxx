CXX = g++
#CXXFLAGS =-g -Wall -Wextra -pedantic-errors `wx-config --cxxflags` 
CXXFLAGS =-g -fpermissive -std=gnu++17 -Wall -Wextra -pedantic-errors `wx-config-gtk3 --cxxflags` 
LDFLAGS = ${shell pkg-config sfml-all --libs} ${shell wx-config-gtk3 --libs} -ltinyxml2
#LDFLAGS =$shell pkg-config sfml-all --libs}  EXAMPLE FOR SFML ALL LIB
SRC = ${wildcard *.cpp}
OBJ = ${SRC:%.cpp=%.o}
EXECUTABLE = wxMapEditor


${EXECUTABLE} : ${OBJ}
	${CXX} ${CXXFLAGS} ${LDFLAGS} ${LIBS} ${OBJ} -o ${EXECUTABLE} 

%.o: %.cpp
	${CXX} ${CXXFLAGS} -c $< -o $@

clean:
	${RM} ${OBJ} ${EXECUTABLE}
