CC = gcc
INC = 
CFLAGS :=-g -Wall -std=c11 -pedantic ${INC}  
# Disable all warnings about unused variables
NO_WARN= -Wno-unused-variable -Wno-unused-function
LDFLAGS = -lfreetype2 
SRC = ${wildcard *.c}
OBJ = ${SRC:%.c=%.o}
EXECUTABLE = ~/EXEC/sfb

# EXAMPLES
# all = start_s.o hey
# hey: one two
# echo $@ →  OUTPUT: hey(This is the target name)
# echo $? →  OUTPUT: All the prerequisites that are newer than the target
# 
# $(TARGET): $(wildcard *.o)
#   ld $? -o $@
#
# start_s.o :start.S 

#   cc -c $< -o $@

${EXECUTABLE} : ${OBJ}
	echo "------------------ LINKING ----------------------"
	${CC} ${OBJ} ${LDFLAGS} -o ${EXECUTABLE} 

%.o: %.c
	${CC} ${CFLAGS} -c $< -o $@

clean:
	${RM} ${OBJ} ${EXECUTABLE}
