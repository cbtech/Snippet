// Basic utilisation of double pointer (pointer to pointer)
// the common usage is for array of char
//
#include <stdio.h>
#include <stdlib.h>

void show_word(const char *value) {
    printf("%c",*value);
}

void show_sentence(const char **value) {
    printf("%s",value[0]);
}
void show_phrase(const char ***value) {
    // value[0][0][0] → %c → H
    // value[0][0]    → %s → Hello, my name is Jack
    //
    printf("%s",value[0]);
}
int main(int argc,char *argv[]) {
    const char *str = "Hello, my name is Jack";       
    show_word(str);
    printf("\n");
    show_sentence(&str);
    const char **new_str = &str;
    show_phrase(&new_str);
    return 0;
}


