// Convert binary to decimal and upset
// gcc -g -Wall bintodec.c -D DECTOBIN -o DECTOBIN
// gcc -g -Wall bintodec.c -D BINTODEC -o BINTODEC
#include <stdio.h>
#include <math.h>

int power(int base, int n) {
	int p = 1;
	for(int i = 1; i <= n; ++i)
		p = p * base;
	return p;
}

int main() {
#if defined DECTOBIN
	int n = 0,mask = 0x80 ;
	printf("Enter Decimal number: ");
	scanf("%d",&n);
	while(mask) {
		printf("%d", (n&mask) ? 1:0);
		mask = mask >> 1;
	}
#elif defined BINTODEC
	int n = 0, k=0, sum = 0;
	printf("Enter Binary number: ");
	scanf("%d",&n);
	while(n > 0) {
		sum +=( n % 10 * power(2,k) );
		k++;
		n /= 10; // Remove 1 number: 1.1100000 
			 // 2.1100000 3.110000 4.11000→
	}
	printf("5 Power 2 = %d\n",pow(2,5));
	printf("The Decimal Value is: %d \n",sum);
#endif
}
		
