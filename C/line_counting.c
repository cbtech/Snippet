#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFSIZE 1024

void get_content(const char* arg,char *buffer)
{
	FILE *stream;

	stream = fopen(arg,"r");

	if(stream == NULL) {
		perror("Error opening file\n");
		exit(EXIT_FAILURE);
	}

	fread(buffer,255,1,stream);
	fclose(stream);
}

int count_char(char *buffer)
{
	int size =0; 
	
	for(; *buffer !='\0'; buffer++)
		size++;

	return size;
}

int count_lines(char *buffer)
{
	int nlines = 0;
	for( ;*buffer != '\0'; buffer++)
		if(*buffer == '\n') 
			nlines++;
	return nlines;
}

void count_words(const char *buffer)
{
	int wcount = 0;

	for(;*buffer != '\0'; ++buffer) {
		if(*buffer == ' ' || *buffer == '\n' || *buffer == '\t' )
			wcount++;
	}
	printf("Words: %d\n",wcount );

}

char* extract_title(const char *buffer)
{
 	char *title = (char*)malloc(256);
	int n = strlen(buffer);
	int start=0,end=0;

	for (int i = 0; i < n ;i++) {
		if(buffer[i] == '>') {
			start = i+1;
			break;
		}
	}
	
	while(buffer[start] == ' ')
		start++;
	
	for(int i = start;i < n ; i++) {
		if(buffer[i] == '<') {
			end = i - 1;
			break;
		}
	}
	int i=0;
 	for(int j = start; j <= end; j++)
	{
		title[i++] = buffer[j]; 	
		// Place \0 at the end to create string
		if(j == end)
			title[i]='\0';
		
	}
	return title;
}

int main(int argc,char *argv[])
{
	if(argc != 2)
		printf("Usage: ./line_counting <file>\n");

	char *buffer = malloc(BUFFSIZE);
	
	get_content(argv[1],buffer);
	int char_nbr = count_char(buffer);
	printf("Char Nbr: %d\n",char_nbr);
	
	int lines_nbr = count_lines(buffer);
	printf("Lines Nbr: %d\n",lines_nbr);
	count_words(buffer);
	// printf("%s\n",buffer); OK PRINT ENTIRE BUFFER

	// Extract Title
	// Allocate title here then send char* to function
	// and deallocate in free function
	char *title = extract_title(buffer);
	printf("TITLE: %s",title);
	exit(EXIT_SUCCESS);
}
