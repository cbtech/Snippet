#include <stdio.h>

int main() {
	char str[100];
	int i,count=0;
	printf("Enter the string: ");
	scanf("%s",str);
	for(i = 0; str[i] != '\0'; i++) {
		if(str[i] == 'a' || str[i] == 'A' ||
		   str[i] == 'e' || str[i] == 'E' ||
		   str[i] == 'i' || str[i] == 'I' ||
		   str[i] == 'o' || str[i] == 'O' ||
		   str[i] == 'y' || str[i] == 'Y') {
			count++; 
		}
	}
	printf("There is %d vowels in string.\n",count);
}
