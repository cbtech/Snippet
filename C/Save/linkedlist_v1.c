// Linked list 
#include <stdio.h>
#include <stdlib.h>
// Version 1: 
// this version works but the first element is NULL
// head points to the first node of the linked list
// next pointer of the last node in NULL.So if the next current node 
// is null, we reached the end of the linked list.

struct linked_list {
	char *data;
	struct linked_list* next;
};

struct linked_list *create_node(struct linked_list *node, char *str) {
	struct linked_list *tmp = malloc(sizeof(struct linked_list ));
	tmp->data = str;
	tmp->next = NULL;
 
    node->next = tmp;
	//*node = *tmp;
	return tmp;
}

struct linked_list *insert_back(struct linked_list *node, char* str) {
    node = create_node(node,str);
    return node;
}

// Display Nodes
void display_nodes(struct linked_list *head, struct linked_list *node) {
   int i = 0;
    struct linked_list *tmp = head;
	for ( ; tmp != NULL; tmp = tmp->next) {
		printf("Element %d: %s\n",i,tmp->data);
		i++;
    }
}

int main() {
	struct linked_list *head,*node;
  	
	node = malloc(sizeof(struct linked_list));
	head = node;	
	// Head point to the first Element
    // [DEBUG]
    //  p head → (struct linked_list *) 0x7ffff7ffe320
	node = create_node(node,"Elem1");
	node = create_node(node,"Elem2");
    //  p &head[2] → (struct linked_list *) 0x7ffff7ffe340
	
	node = create_node(node,"Elem3");
	node = create_node(node,"Elem4");
	node = create_node(node,"Elem5");
	display_nodes(head,node);
	return 0;
}

