// 
// 
// 
//
//
//
//

#include <stdio.h>

int main(int argc,char *argv[])
{
	FILE *file = fopen(argv[1], "rb");

	// Get the size of the text, data, and BSS Section
	int plateform,text_size, data_size, bss_size;
	
	// Read plateform
	fseek(file,0x04,SEEK_SET);
	fread(&plateform,sizeof(int), 1, file);
	fseek(file, 0x20, SEEK_SET);
	fread(&text_size, sizeof(int), 1, file);
	fseek(file, 0x30, SEEK_SET);
	fread(&data_size, sizeof(int), 1, file);
	fseek(file,0x34, SEEK_SET);
	fread(&bss_size, sizeof(int), 1, file);

	// Print the size
	printf("Plateform: %d\n Text Size: %d\nData Size: %d\nBSS Size: %d\n", plateform,text_size, data_size, bss_size);
	return 0;
}
