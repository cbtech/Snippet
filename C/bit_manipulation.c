#include <stdio.h>

// How to convert to Binary
//  5 to binary
//  dividend 	remainder
//   5/2 = 2 	2*2 = 4  	→ 5-4 = 1
//   2/2 = 1	1*2 = 2		→ 2-2 = 0 
//   1/2 = 0 	0*2 = 2		→ 1-0 = 1
//   5 is 101
//
//   9 to binary
//   dividend 	remainder 
//   9/2 = 4	4 * 2 = 8 	→ 9-8 = 1
//   4/2 = 2	2 * 2 = 4	→ 4-4 = 0
//   2/2 = 1	1 * 2 = 2	→ 2-2 = 0
//   1/2 = 0	0 * 2 = 0	→ 1-0 = 1

// We use bitwise OR operator | to set a bit.
// x |= (1U << pos) it will set nth bit.
#define SET_BIT(x,pos) (x |= (1U << pos))

// We use the bitwise AND operator & 
// to clear a bit. 
// x &= ~(1UL << pos);
#define CLEAR_BIT(x,pos) (x &= (~(1U << pos)))

// We use the bitwise XOR operator ^ 
// to toggle a bit.
// x^=1U << pos;
#define TOGGLE_BIT(x,pos) x ^= (1U << pos)

void binary(unsigned int n)
{
	unsigned mask=0x80;
	//unsigned mask=0x80000000;
	while(mask) {
		printf("%d", (n&mask)?1:0);
		mask = mask >> 1;
	}
}

int main(int argc,char *argv[])
{
	// a = 5(00000101),b=9(00001001)
	unsigned char a = 5, b = 9;
	
	printf("\ta = %d, b = %d",a,b);
	printf("\t\tA: "); binary(a);
	printf("\tB: "); binary(b);
	printf("\n");

	// The result is 00000001
	printf("\ta & b = %d\t", a & b);
	printf("\ta & b: " ); binary(a&b);
	printf("\n");

	// The result is 00001101
	printf("\ta | b = %d\t", a | b);
	printf("\ta | b: "); binary(a|b);
	printf("\n");

	// The result is 000010010
	// b = 00001001 then b << 1 
	printf("\tb << 1 = %d\t", b << 1);
	printf("\tb << 1:"); binary(b << 1);
	printf("\n");


	// The result is 00000100
	// b=00001001 then b >> 1 → 00000100
	printf("\tb >> 1 = %d\t", b >> 1);
	printf("\tb >> 1:"); binary(b >> 1);
	printf("\n\n");
	
	printf("\tTOGGLE BIT \n");
 	// a = 00000101 after 001←(5th)00101 ←(0th)
	printf("\t\tBefore: "); binary(a);
	unsigned int num = TOGGLE_BIT(a,5);
	printf("\t\tToggle 5th bit of a: "); binary(num);
	printf("\n");

	printf("\tCLEAR BIT\n");
	printf("\t\tBefore: "); binary(a);
	num = CLEAR_BIT(a,0);
	printf("\t\tClear 0th bit of a: "); binary(num);
	printf("\n");

	printf("\tSET BIT\n");
	printf("\t\tBefore: "); binary(a);
	num = SET_BIT(a,2);
	printf("\t\tSet 2th bit of a: "); binary(num);
	printf("\n");

	return 0;
}
