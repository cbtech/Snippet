//
//  Writing struct to file with fread / fwrite
//  For writing example 
//     gcc -g -Wall -w file.c -DWRITE -o WRITE_EXAMPLE
//  For reading example 
//	gcc -g -Wall -w file.c -DREAD -o READ_EXAMPLE
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person {
   int id;
   char fname[20];
   char lname[20];
};

int main(int argc, char *argv[])
{

 	#if defined(WRITE)
	  FILE *outfile;
	  outfile = fopen("person.dat","w+");

	  if(outfile == NULL) {
  		fprintf(stderr,"\nError opened file\n");
		exit (1);
  	  }

	  struct person input1 = { 1, "Rohan", "Sharma"};
	  struct person input2 = { 2, "Mahendra", "Dhoni"};

	  // WRITE STRUCT TO FILE
	  fwrite(&input1, sizeof(struct person), 1, outfile);
	  fwrite(&input2, sizeof(struct person), 1, outfile);

	  if(fwrite != 0)
		printf("Contents  to file written successfully! \n");
	  else 
		printf("Error writing file! \n");

	  fclose(outfile);
	#elif defined(READ) 
   	  FILE *infile;	
	  struct person input;

	  infile = fopen("person.dat","r");
	  if(infile == NULL) {
		  fprintf(stderr,"\n Error opening file\n");
		  exit(1);
	  }

	  // READ FILE
	  while(fread(&input,sizeof(struct person), 1, infile))
		  printf("id = %d, Name = %s %s\n",input.id,input.fname, input.lname);
	  fclose(infile);

	#endif

	return 0;

}
