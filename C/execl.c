// Simple execl programm
// gcc -g -std=c11 -Wall execl.c -o EXECL

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


int main(int argc,char *argv[]) {
	char *path,*arg0,*arg1,*arg2;
	path = "/usr/bin/echo";
	arg0 = "NULL";
	arg1 = "Hello";
	arg2 = "Jack";

  const char *argX[] = {"NULL","Hello","Jack" };
  // execl(pathname,arg,....)
	// if(execl(path,arg0,arg1,arg2,NULL) == -1) 
  // Output: Hello Jack

  if(execl(path,argX[0],argX[1],argX[2],NULL) == -1) 
	    printf("Error: exec %s \n",strerror(errno) );
	
	return 0;
}
