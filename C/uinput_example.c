#include <stdio.h>
#include <fcntl.h>
#include <sys/io.h>
#include <linux/uinput.h>

void emit(int fd, int type, int code, int val)
{
	struct input_event ie;
	  
	// Type is: EV_REL for relative movement EV_KEY for a keypress or release
	// code is event code (example: REL_X or KEY_BACKSPACE)
	// value: 0 released / 1 pressed / 2 autorelease 
	ie.type = type;
	ie.code = code;
	ie.value = val;

	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	write(fd, &ie, sizeof(ie));
}

int main(int argc,char *argv[]) 
{
	struct uinput_setup usetup;
	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_USB;
	usetup.id.vendor = 0x1342;
	usetup.id.product = 0x9876;
	strcpy(usetup.name, "Virtual Keyboard");


	int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	
	/* SEND CTRL+X 
	 *   int CTRL_KEY = 0x1d;
	 *   int X_KEY = 0x2d;
	 *   
	 *   ioctl(fd,UI_SET_KEYBIT,CTRL_KEY);
	 *   ioctl(fd,UI_SET_KEYBIT,X_KEY);
	 *   
	 *   emit(fd,EV_KEY,CTRL_KEY, 1);
	 *   emit(fd,EV_SYN,SYN_REPORT,0);
	 *
	 *   emit(fd,EV_KEY,X_KEY,1);
	 *   emit(fd,EV_SYM,SYN_REPORT,0);
	 *
	 * UNLEASH X / CTRL
	 *   emit(fd,EV_KEY,CTRL_KEY,0);
	 *   emit(fd_EV_SYN,SYN_REPORT,0);
	 *
	 *   emit(EV_KEY,X_KEY,0);
	 *   emit(EV_SYN,SYN_REPORT,0);
	 */

 	int KEY_TEST = 43;
	ioctl(fd,UI_SET_EVBIT,EV_KEY);
	ioctl(fd,UI_SET_KEYBIT, KEY_TEST);

	ioctl(fd, UI_DEV_SETUP, &usetup);
	ioctl(fd,UI_DEV_CREATE);

	sleep(1);

	// Key press, Report the event, Send key release, and report again
	emit(fd,EV_KEY, KEY_TEST, 1);
	emit(fd,EV_SYN, SYN_REPORT,0);
	emit(fd,EV_KEY, KEY_TEST, 0);
	emit(fd,EV_SYN, SYN_REPORT, 0);
	sleep(1);

	// Destroy the event
	ioctl(fd, UI_DEV_DESTROY);
	close(fd);
	return 0;
}

