// 
// Simple Server for communication using uinput
// cc -g -Wall IPC_uinput_server.c -o IPC_uinput_server
//

#include <stdio.h>
#include <linux/uinput.h>
#include <fcntl.h>	// O_WRONLY
#include <sys/socket.h>
#include <sys/un.h> 	// sun_family

static const char *socket_path = "/tmp/1000-runtime-dir/waydotool.sock";
static const char *socket_perm = "0607";

static void uinput_setup(int fd) {
	static const int event_list[] = { EV_KEY, EV_REL };

	for (int i = 0; i < sizeof(event_list) / sizeof(int); i++)
	{
		if(ioctl(fd, UI_SET_EVBIT,event_list[i])) 
			fprintf(stderr, "UI_SET_EVBIT %d failed\n", i);
	}

	static const int key_list[] = { KEY_ESC, KEY_1, KEY_2,KEY_3,KEY_4,KEY_5,KEY_6,KEY_7,KEY_9,KEY_O};

	for (int i = 0; i < sizeof(key_list) / sizeof(int); i++)
	{
		if(ioctl(fd, UI_SET_KEYBIT, key_list[i]))
			fprintf(stderr,"UI_SET_KEYBIT %d failed\n",i);
	}

	struct uinput_setup usetup;

	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_VIRTUAL,
	usetup.id.vendor = 0x2333,
	usetup.id.product = 0x6666,
	usetup.id.version = 1,
	strcpy(usetup.name, "Wdt Virtual device");

	if (ioctl(fd,UI_DEV_SETUP,&usetup)) 
		fprintf(stderr,"UI_DEV_SETUP ioctl failed\n");

	if(ioctl(fd,UI_DEV_CREATE)) 
		fprintf(stderr,"UI_DEV_CREATE ioctl failed\n");
}

int main(int argc , char *argv[])
{
	int fd_ui = open("/dev/uinput", O_WRONLY);

	if (fd_ui < 0) 
		fprintf(stderr,"Failed to open uinput device");

	uinput_setup(fd_ui);
	
	// SOCK_DGRAM provides connectionless 
	// communication between two systems
	int fd_so = socket(AF_UNIX, SOCK_DGRAM, 0);
	if( fd_so < 0)
		fprintf(stderr, "Failed to create socket\n");
	
	unlink(socket_path);

	struct sockaddr_un socket_addr;
	socket_addr.sun_family = AF_UNIX;

	strncpy(socket_addr.sun_path, socket_path, sizeof(socket_addr.sun_path) -1);

	if(bind(fd_so, (const struct sockaddr *) &socket_addr,
				sizeof(socket_addr))) {
		fprintf(stderr,"Unable to bind socket\n");
	}
	
	chmod(socket_path,strtol(socket_perm,NULL,8));
	struct input_event ie;

	while(1) {
		if(recv(fd_so,&ie,sizeof(ie), 0) == sizeof(ie)) {
			write(fd_ui, &ie,sizeof(ie));
			printf("input event received : value: %d  ,Code:  %d", ie.code,ie.value );
		}
	}

	return 0;
}
