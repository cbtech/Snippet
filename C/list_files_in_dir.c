//
// List files in directory in C
//  cc -g -Wall -w list_files_in_dir.c -o List_files_in_dir
//
//

#include <stdio.h>
#include <dirent.h>	// DIR

void listFiles(const char *dirname)
{
	DIR* dir=opendir(dirname);
	if(dir == NULL)
		fprintf(stderr,"Failed to open directory\n" );

	struct dirent* entity;
	entity = readdir(dir);
	while(entity != NULL) {
		printf("%hhd %s/%s\n",entity->d_type, dirname, entity->d_name);

		// If it is directory... Show dirent.h to show differents options
		// strcmp return to parent in each iteration
		if(entity->d_type == DT_DIR && strcmp(entity->d_name, ".") && strcmp(entity->d_name,".." ) !=0){
			char path[100] = { 0 };
			strcat(path, dirname);
			strcat(path, "/");
			strcat(path, entity->d_name);
			listFiles(path);
		}

		entity = readdir(dir);
	}

	closedir(dir);

}

int main(int argc, char *argv[])
{
	listFiles(".");
	return 0;
}
