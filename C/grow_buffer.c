// Create buffer using malloc 
// Grow buffer to store file (read_Inputdevice[mouse].c ~100LOC)
// This a test for valgrind
 
// gcc -g -Wall growbuffer.c -o GROWBUFFER
//

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))
#define FILE_BYTESIZE 2260

struct buffer_t {
	char *content;
	size_t size;
};

size_t grow_buffer(struct buffer_t *buffer,size_t size,size_t nb_elems)
{
	size_t real_size = size * nb_elems;
	char *ptr = realloc(buffer->content, FILE_BYTESIZE);
	if(ptr)  {
		buffer->content = ptr;
		buffer->size += real_size;
	}

	else {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}

	return real_size;
}

void read_file(FILE* file,struct buffer_t *buffer) {
	fread(buffer->content,sizeof(char),FILE_BYTESIZE,file);
}

int main() {
	FILE* fp = NULL;

	fp = fopen("read_Inputdevice[mouse].c","r");
	if(!fp) { printf("Error opening file\n"); }

	struct buffer_t *buffer = malloc(sizeof(struct buffer_t));
	buffer->content = malloc(256);
	grow_buffer(buffer,1,FILE_BYTESIZE);
	read_file(fp,buffer);
	printf("%s",buffer->content);
	fclose(fp);

	free(buffer->content);
	free(buffer);
	return 0;
}
