//
//  How to read an array of struct in C
//  
// 	cc -g -Wall -w array_of_struct.c -o Array_of_struct
//	Additional file: input.cc


#include <stdio.h>

typedef struct Point {
  int x,y;
  int val;
} Point;

int main(int argc, char*argv[])
{
	FILE* file = fopen("input.cc","r");

	if (file == NULL) 
		fprintf(stderr,"Error opening file\n");

	char buffer[200];
	fgets(buffer, 200, file);

	Point points[100];
	int i = 0;
	while (!feof(file))
	{
		Point *p = points + i ;
		sscanf(buffer, "%d %d %d", &p->x, &p->y, &p->val);
		fgets(buffer,200,file);
		i++;
	}

	int n = i;
	for ( i = 0; i < n; i++)
	{
		printf("Read point: %d %d %d \n", points[i].x, points[i].y, points[i].val);
	}
	fclose(file);
	return 0;
}
