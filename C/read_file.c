#include <stdio.h>
#include <time.h>
#include <string.h>

const char *file = "/tmp/wtimer";

// 45 MINUTES = 46x60 =  2760 SECS
const int work_time = 2760;

typedef struct {
	int start_time;
	char *state;
	int timer;
} pomodoro;


int get_time()
{
	return time(NULL); 
}

int get_start_time(pomodoro *p)
{
	return p->start_time;
}

void decrement_timer(pomodoro* p)
{
	int current_time = get_time();
        int diff = current_time - p->start_time;
	p->timer = work_time - diff;
}

void incremente_timer(pomodoro *p)
{
}

void compute(pomodoro* p)
{
	decrement_timer(p);
	if(!strcmp(p->state,"WORK"))
		printf("%s: %d ",p->state,p->timer /60);
	else 
		printf("%s",p->state);
}

void write_file(pomodoro *p)
{
	FILE *fp = fopen(file,"w");
	if(!strcmp(p->state,"PAUSE"))
		printf("START_TIME: %d, STATE: %s", get_start_time(p),p->state);

	fprintf(fp,"%d,%s", p->start_time,p->state);

	fclose(fp);

}

void read_file(pomodoro *p)
{
	FILE *fp = fopen(file,"r");
	fscanf(fp,"%d,%s",&p->start_time,p->state);
	fclose(fp);
}

int main(int argc,char *argv[])
{
	pomodoro p;
	
	if (!strcmp(argv[1],"start")) {
		p.start_time=get_time();
		p.state = "WORK";
		write_file(&p);
	}

	else if (!strcmp(argv[1],"status")){
		read_file(&p);
		compute(&p);
	}
	else if (!strcmp(argv[1],"pause")) {
		p.state = "PAUSE";
		write_file(&p);
	}

	return 0;
}
