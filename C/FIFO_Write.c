/* 
 *
 * FIFO communication between process
 *
 */

#include <stdio.h>
#include <fcntl.h>	// O_WRONLY
#include <unistd.h>	// write
#include <sys/types.h>	//mkfifo
#include <stdlib.h>

#include <time.h>

int main(int argc,char *argv[])
 {
	 int arr[5];
	 srand(time(NULL));
	 char *fifo_file = "/tmp/fifo";
	 mkfifo(fifo_file,0666);

	 for (int i = 0; i < 5; i++)
	 {
		 arr[i] = rand()  % 100;
		 printf("Generated %d\n",arr[i]);
	 }

	 int fd = open(fifo_file,O_WRONLY);
	 if (fd == -1) {
		return 1;
	 }

	if(write(fd,arr,sizeof(int) * 5 ) == -1) {
		fprintf(stderr,"Error: Writing file is not possible\n");
	 }

	 close(fd);

	 return 0;
 }
