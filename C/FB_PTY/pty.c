// Simple Pseudo terminal utilisation in C
// Don't render anything, just create a process using fork 
// and assign different var env
// gcc -g -std=c11 -Wall pty.c -o PTY
// 

// Don't forget to put the define below in first inclusion
#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/select.h>


struct pseudo_term {
	int master_fd;
	int slave_fd;
	int perm;
	char *slave_name;
};

int open_master(struct pseudo_term *pts) {
	if ((pts->master_fd = open("/dev/ptmx",O_RDWR | O_NOCTTY)) == -1 
	|| grantpt(pts->master_fd) == -1
	|| unlockpt(pts->master_fd) == -1 
	|| (pts->slave_name = ptsname(pts->master_fd)) == NULL) {
		fprintf(stderr,"[ERR] Cannot open master(pty.c L28)\n");
			return -1;
	}
	return 0;
}

pid_t spawn_pty(struct pseudo_term *pts) {
	struct termios slave_termios;
	struct winsize *ws;

	memset (&ws,0,sizeof(ws));
	
	open_master(pts);

	pid_t child_pid = fork();
	if(child_pid == -1) { // ERROR
		fprintf(stderr,"[ERR]Cannot fork(pty.c L50)\n");
		exit(EXIT_FAILURE);
	}	

	else if(child_pid != 0) { 	
		return EXIT_FAILURE;
	}
	
	if(setsid() < 0)
		fprintf(stderr,"[ERR]Cannot start session setsid(pty.c L60)\n");
	
	pts->slave_fd = open(pts->slave_name,O_RDWR);
	
	// Get terminal attributes
	if(tcgetattr(STDIN_FILENO,&slave_termios) <0)  
		fprintf(stderr,"[ERR]Cannot get terminal attributes(pty.c L65)\n");
	
	slave_termios.c_lflag  &= ~(ICANON | ISIG | IEXTEN | ECHONL | ECHO);
	
	// Set Terminal attributes
	if(tcsetattr(pts->slave_fd,TCSANOW,&slave_termios) == -1)
		fprintf(stderr,"[ERR] Setting term(pty.c L68)\n");

	if(ws != NULL)  {
		if(ioctl(pts->slave_fd, TIOCSWINSZ, ws) == -1)
			fprintf(stderr,"[ERR] setting winsize(pty.c L73)\n");
	}

	if(dup2(pts->slave_fd, STDIN_FILENO) != STDIN_FILENO ||
	   dup2(pts->slave_fd, STDOUT_FILENO) != STDOUT_FILENO ||
	   dup2(pts->slave_fd, STDERR_FILENO) != STDERR_FILENO) {
		fprintf(stderr,"[ERR] Cannot duplicate slave(pty.c L83)\n");
	}

	close(pts->master_fd);
	close(pts->slave_fd);
	return 0;
}

const char *shell_cmd="/bin/bash";
int exec_term(struct pseudo_term* pts) {
	pid_t pid;
	pid = spawn_pty(pts);
	
	char *shell = NULL;
	if(pid < 0) { // ERROR
		return 0;
	}
	else if(pid == 0) { //Child
		shell = getenv("SHELL");
		execl(shell_cmd,shell_cmd,(char*)NULL);
	exit(EXIT_FAILURE);
	}
	return 1;
}

void release_and_close(struct pseudo_term *pts) {
	close(pts->master_fd);
	free(pts);	
}

int main(int argc,char **argv) {
	struct pseudo_term *pts;
	pts = malloc(sizeof(struct pseudo_term));
	
	const int BUFFER_SIZE=256;
	char buffer[BUFFER_SIZE];
	size_t n_read;	
	fd_set fds;

	// Create a child process that is connected to 
	// the parent using pseudoterminal_pair
	exec_term(pts);
	
	for(;;) {
		// macro removes all fd from set
		FD_ZERO(&fds);
		// macro adds the fd to set
		FD_SET(STDIN_FILENO,&fds);
		FD_SET(pts->master_fd,&fds);
		
		// Select allows a app to monitor multiple 
		// fd, waiting until one or more of the fd
		// become "ready"
		if(select(pts->master_fd +1, &fds,
				NULL, NULL, NULL) == -1) 
			printf("Error Select line 117\n");
		
		if(FD_ISSET(STDIN_FILENO,&fds)) {
			n_read = read(STDIN_FILENO,buffer,BUFFER_SIZE);
			if(write(pts->master_fd,buffer, n_read) != n_read)
				printf("Failed to write (STDIN_FILENO)\n");
		}

		if(FD_ISSET(pts->master_fd,&fds) ) {
			n_read = read(pts->master_fd,buffer,BUFFER_SIZE);
			if(write(STDOUT_FILENO,buffer,n_read) != n_read)
				printf("Failed to write (STDOUT_FILENO\n");
		}
	}

	// Close properly
	release_and_close(pts);
	return 0;
}
