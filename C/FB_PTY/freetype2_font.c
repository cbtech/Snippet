// Simple Freetype2 example 
// gcc -g -Wall freetype2_font.c -I/usr/include/freetype2/ -lfreetype -o FREETYPE2
// https://kevinboone.me/fbtextdemo.html?i=2

#include <stdio.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#define WIDTH 1920
#define HEIGHT 1080

int main() {
	FT_Library library;

	int pixel_size = 20;

	if(FT_Init_FreeType(&library)) {
		fprintf(stderr,"Error init font library\n");
	}

	FT_Face face;
	
	if(FT_New_Face(library,"/usr/share/fonts/TTF/DejavuSans.ttf", 0, &face)) {
			fprintf(stderr,"Error Loading font\n");
	}

	

	if(FT_Set_Pixel_Sizes(face, 0, pixel_size)) {
		fprintf(stderr,"Error setting font size\n");
	}
	FT_ULong character= 'A';
	FT_UInt gi = FT_Get_Char_Index(face, character);
	FT_Load_Glyph(face, gi,FT_LOAD_DEFAULT);
	
	int bbox_ymax = face->bbox.yMax / 64;
	int glyph_width = face->glyph->metrics.width / 64;
	int advance = face->glyph->metrics.horiAdvance / 64;
	int x_off = (advance - glyph_width) / 2;
	int y_off = bbox_ymax - face->glyph->metrics.horiBearingY / 64;

	FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

	// x and y are the coordinates of the current drawing origin, starting
//  at the top-left of the bounding box.
	int x = 10;
	int y = 10;
	for (int i = 0; i < (int)face->glyph->bitmap.rows; i++)
    	{
 		  // row_offset is the distance from the top of the framebuffer
    		  //   of the text bounding box
 	  	  int row_offset = y + i + y_off;
 		  for (int j = 0; j < (int)face->glyph->bitmap.width; j++) {
      			unsigned char p = face->glyph->bitmap.buffer [i * face->glyph->bitmap.pitch + j];

     	 // Don't draw a zero value, unless you want to fill the bounding
      	//   box with black.
      	if (p)
        	framebuffer_set_pixel (fb, x + j + x_off, row_offset, p, p, p);
      	}
    	}
	  // Move the x position, ready for the next character.
	  x += advance;
		v/*
	if(FT_Load_Char(face, 'A', FT_LOAD_RENDER)) {
		fprintf(stderr, "Error loading char A\n");
	}
	
        if(FT_Load_Glyph( face, FT_Get_Char_Index( face, 'A' ), FT_LOAD_DEFAULT )) {
		fprintf(stderr,"Error Load Glyph\n");
	}

	FT_Render_Mode render_mode = FT_RENDER_MODE_NORMAL;
	FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
	error = FT_Render_Glyph( face->glyph,render_mode ); 	*/
	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return 0;
}
