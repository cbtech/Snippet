// Simple Framebuffer example (Fill screen with RGB Color)
// gcc -g -Wall -sfb.c -o SFB

#include <stdio.h>
#include <stdlib.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

struct framebuffer {
	int fd;
	int width;
	int height;
	int bytes;
	int data_size;
	int line_length;
	int stride;
	int slop;
	
	char* data;

};

int main() {
	struct framebuffer *fb;
	fb = malloc(sizeof(struct framebuffer));

	fb->fd = open("/dev/fb0",O_RDWR);

	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo vinfo;

	ioctl(fb->fd,FBIOGET_FSCREENINFO,&finfo);
	ioctl(fb->fd,FBIOGET_VSCREENINFO,&vinfo);
	
	fb->width = vinfo.xres;
	fb->height = vinfo.yres;
	// bits_per_pixel = 32, each pixel has 4bytes in size 
	fb->bytes = vinfo.bits_per_pixel / 8;

	// Calculate the amount of memory it occupies
	fb->data_size = fb->width * fb->height * fb->bytes;
	
	fb->data = mmap(0, fb->data_size,
			PROT_READ | PROT_WRITE, MAP_SHARED,
			fb->fd, (off_t)0);
	
	// Read or write the mapped Screen
	// Blank the entire screen set the area to 0
	memset(fb->data, 0, fb->data_size);

	// 16 Colors bar
//	for(int x= 0; x < fb->width; x++) {
//		for(int y = 0; y < fb->height; y++) {

		int offset = (x * 10 + y * fb->width) * fb->bytes;
		unsigned char r = 0;
		unsigned char g = 45;
		unsigned char b = 54;

		fb->data[offset + 0] = b;
		fb->data[offset + 1] = g;
		fb->data[offset + 2] = r;
//		}
//	}

	// Clean up the framebuffer
    	int fb_height = 0;
	munmap (fb->data, fb->data_size);
	close(fb->fd);
}
