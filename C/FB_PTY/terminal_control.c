// Disabling echo terminal 
// stty -a
// gcc -g -Wall -std=c11 terminal_control.c -o ~/EXEC/TERM_CTL

#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <unistd.h> //isatty STDIN_FILENO
#include <sys/ioctl.h>

#define STDIN_FILENO 0

int main() {
	
	struct termios term;
	struct winsize ws;

	int fd = 0;

	if(isatty(STDIN_FILENO))
		fd = STDIN_FILENO;

	if(tcgetattr(fd,&term) == -1)
            fprintf(stderr,"Error Getting terminal attributes\n");
        term.c_lflag &= ~(ECHO |ICANON | ISIG | IEXTEN);
        if(tcsetattr(fd,TCSANOW,&term) == -1) 
            fprintf(stderr,"Error Setting terminal attributes\n");
	return 0; 
}
