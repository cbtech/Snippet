/*
 * Hexadecimal  256 colors palette
 *
 *  tcc -ggdb3 -Wall minimal_cmap.c -o ~/EXEC/MINIMAL_CMAP
 *  doc kernel linux : drivers/video/fbdev/core/fbcmap.c
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <errno.h>

struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

#define COLOR_LENGTH 16
/*
static const char *colorname[] = {
    "#000000",
    "#FF0000",
    "#0000FF",
    "#FFFC00",
    "#0000FF",
    "#FF00E9",
    "#00FFE0",
    "#FFFFFF",
    "#000000",
    "#FF0000",
    "#0000FF",
    "#FFFC00",
    "#0000FF",
    "#FF00E9",
    "#00FFE0",
    "#FFFFFF"
};
*/

//MONOKAI THEME
static const char *colorname[] = {
 "#272822",
 "#f92672",
 "#a6e22e",
 "#f4bf75",
 "#66d9ef",
 "#ae81ff",
 "#a1efe4",
 "#f8f8f2",
 "#75715e",
 "#f92672",
 "#a6e22e",
 "#f4bf75",
 "#66d9ef",
 "#ae81ff",
 "#a1efe4",
 "#f9f8f5"
};

struct color_array {
    uint16_t red[COLOR_LENGTH];
    uint16_t green[COLOR_LENGTH];
    uint16_t blue[COLOR_LENGTH];
};

void parse_colormap(struct color_array *ca)
{
    unsigned int r;
    unsigned int g;
    unsigned int b;

    for (int i = 0; i < COLOR_LENGTH; i++) {
        sscanf(colorname[i], "#%2x%2x%2x", &r, &g, &b);
        printf("Color %d: %s\n", i, colorname[i]);

       ca->red[i] = r;
        ca->green[i] = g;
        ca->blue[i] = b;
    
   }
}

void show_color_info(uint16_t * color, size_t len, char *colorname)
{
    printf("[%s]\t", colorname);
    for (int i = 0; i < len; i++) {
        printf("%d\t", color[i] / 256);
    }
    printf("\n");
}

void get_fb_information(int *framebuffer_fd,
                        struct fb_var_screeninfo *vinfo,
                        struct fb_fix_screeninfo *finfo)
{

    // Get variable screen information
    if (ioctl(*framebuffer_fd, FBIOGET_VSCREENINFO, vinfo)) {
        printf("Error reading variable information.\n");
    }
    // Get fixed screen information
    if (ioctl(*framebuffer_fd, FBIOGET_FSCREENINFO, finfo)) {
        printf("Error reading fixed information.\n");
    }
}

void cmap_init(struct fb_cmap *cmap)
{
    cmap->transp = 0;
    cmap->start = 0;
    cmap->len = 16;
}

void get_cmap(int *framebuffer_fd, struct fb_cmap *cmap)
{
    // Get color map
    if (ioctl(*framebuffer_fd, FBIOGETCMAP, *cmap) == -1)
        printf("Error FBIOGETCMAP %s\n", strerror(errno));
}

void set_cmap(int *framebuffer_fd, struct fb_cmap *cmap, struct color_array *ca)
{
    unsigned short r[256];
    unsigned short b[256];
    unsigned short g[256];

    // Set colormap
    for (int i = 0; i < cmap->len; i++) {
        r[i] = ca->red[i] << 8;
        g[i] = ca->green[i] << 8;
        b[i] = ca->blue[i] << 8;
    }

    if (ioctl(*framebuffer_fd, FBIOPUTCMAP, cmap) == -1)
        printf("Error FBIOPUTCMAP %s\n", strerror(errno));
}

void draw_square(int *framebuffer_fd, struct color_array *ca)
{
    int i, x, y;

    const int TILE_WIDTH = 32;
    const int MAX_TILE = 282;
    const int MAX_HEIGHT = 132;

    size_t data_size = vinfo.xres * vinfo.yres * (vinfo.bits_per_pixel / 8);

    char *data = mmap(0, data_size, PROT_READ | PROT_WRITE,
                      MAP_SHARED, *framebuffer_fd, (off_t) 0);

    int fb_bytes = vinfo.bits_per_pixel / 8;

    // Draw 32x32 for each color
    for (i = 0; i < COLOR_LENGTH; i++) {
        for (y = 100; y < MAX_HEIGHT; y++) {
            for (x = 250 + i * TILE_WIDTH;
                 x < MAX_TILE + (i * TILE_WIDTH); x++) {
                int offset = (x + y * vinfo.xres) * fb_bytes;

                unsigned char r = ca->red[i];
                unsigned char g = ca->green[i];
                unsigned char b = ca->blue[i];

                data[offset + 0] = b;
                data[offset + 1] = g;
                data[offset + 2] = r;
            }
        }
    }
}

void alloc_cmap_colors(struct fb_cmap *cmap)
{
    cmap->red = malloc(sizeof(uint16_t));
    cmap->green = malloc(sizeof(uint16_t));
    cmap->blue = malloc(sizeof(uint16_t));
}

void release_cmap_colors(struct fb_cmap *cmap)
{
    free(cmap->red);
    free(cmap->green);
    free(cmap->blue);
}

int main() {
    int framebuffer_fd = 0;

    struct fb_cmap cmap;
    alloc_cmap_colors(&cmap);
    cmap_init(&cmap);

    struct color_array c_array;

    // Open the file for reading and writing
    framebuffer_fd = open("/dev/fb0", O_RDWR);

    if (framebuffer_fd == -1) {
        printf("Error: cannot open framebuffer device.\n");
        release_cmap_colors(&cmap);
        return (1);
    }
    // Get information about Framebuffer
    get_fb_information(&framebuffer_fd, &vinfo, &finfo);
    parse_colormap(&c_array);

    get_cmap(&framebuffer_fd, &cmap);
    for (int i = 0; i < cmap.len; i++)
        printf("\tcolor%d", i);
    printf("\n");

    set_cmap(&framebuffer_fd, &cmap, &c_array);
    show_color_info(cmap.red, cmap.len, "RED");
    show_color_info(cmap.green, cmap.len, "GREEN");
    show_color_info(cmap.blue, cmap.len, "BLUE");

    draw_square(&framebuffer_fd, &c_array);
    // close fb file
//    release_cmap_colors(&cmap);
    close(framebuffer_fd);

    return 0;
}
