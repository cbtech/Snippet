// Simple termios keycode example 
// using termios 
//
// tcc -g -Wall -std=c11 terminal_key.c -o EXEC/TKEY

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <unistd.h>
#include <linux/input.h>
#include <linux/kd.h>

static int terminal_descriptor = -1;
static struct termios terminal_settings;

int main(int argc, char *argv[])
{
	char buffer[18];
	if(isatty(STDIN_FILENO))
		terminal_descriptor = STDIN_FILENO;

	tcgetattr(terminal_descriptor,&terminal_settings);
	// Enable non cannonical mode
	terminal_settings.c_lflag &= ~(ICANON | ECHO );

	terminal_settings.c_cc[VTIME] = 1;
	terminal_settings.c_cc[VMIN] = sizeof(buffer);
	// TCSANOW change occurs immediately 	
	tcsetattr(terminal_descriptor,TCSANOW, &terminal_settings);
	
	int fd=0;
	size_t n;
	int i;

	while(1) {
		n =read (fd,buffer,sizeof(buffer));
		i = 0;
		
		while(i < n) {
			int kc;
			const char *s;
			s = (buffer[i] & 0x80) ? ("release") : ("Press");
			if (i + 2 < n && (buffer[i] & 0x7f) == 0
			&& (buffer[i + 1] & 0x80) != 0 
			&& (buffer[i + 2] & 0x80) != 0)
			{
				kc = ((buffer[i+1] & 0x7f) << 7) |
					(buffer[i + 2] & 0x7f);
				i+=3;

			}
			else {
				kc = (buffer[i] & 0x7f);
				i++;
			}
				printf("Keycode : %3d %s\n", kc,s);
		}


	}
}


