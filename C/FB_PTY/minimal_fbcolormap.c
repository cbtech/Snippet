/*
 * Simple 16 colorpalette 
 *
 *  gcc -ggdb3 -Wall minimal_cmap.c -o ~/EXEC/MINIMAL_CMAP
 *  doc kernel linux : drivers/video/fbdev/core/fbcmap.c
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <errno.h>

struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

#define MAX_WIDTH 512
#define MAX_HEIGHT 32


void get_fb_info(struct fb_var_screeninfo *vinfo,struct fb_fix_screeninfo *finfo) {
    printf("xres: %d \t yres: %d \tbpp: %d\n", 
            vinfo->xres, vinfo->yres, 
            vinfo->bits_per_pixel );
    printf("Visual info: %d\n",finfo->visual);
    printf("Line length: %d\n",finfo->line_length);
    printf("vinfo.vmode is: %d\n",vinfo->vmode);
    printf("vinfo.red.offset=0x%x\n",vinfo->red.offset);
    printf("vinfo.red.length=0x%x\n",vinfo->red.length);
    printf("vinfo.green.offset=0x%x\n",vinfo->green.offset);
    printf("vinfo.green.length=0x%x\n",vinfo->green.length);
    printf("vinfo.blue.offset=0x%x\n",vinfo->blue.offset);
    printf("vinfo.blue.length=0x%x\n",vinfo->blue.length);
    printf("vinfo.transp.offset=0x%x\n",vinfo->transp.offset);
    printf("vinfo.transp.length=0x%x\n",vinfo->transp.length);
    
}

//16 colors
static uint16_t default_r16[] = {
    0, 170, 0, 170, 0, 170,
	0, 170, 85, 255, 85, 255,
	85, 255, 85, 255
};

static uint16_t default_g16[] = {
	0, 0, 170, 85, 0, 0,
	170, 170, 85, 85, 255, 255,
	85, 85, 255, 255
};

static uint16_t default_b16[] = {
    0, 0, 0, 0, 170, 170,
	170, 170, 85, 85, 85, 85,
	255, 255, 255, 255
};

void show_color_info(uint16_t* color,size_t len,char *colorname) {
        printf("[%s]\t",colorname);
    for(int i = 0; i < len; i++) {
        printf("%d\t" ,color[i] /256);
    }
    printf("\n");
}

int main(int argc, char* argv[])
{
    int framebuffer_fd = 0;
    struct fb_cmap cmap;

    // Open the file for reading and writing
    framebuffer_fd = open("/dev/fb0", O_RDWR);
    if (framebuffer_fd == -1) {
        printf("Error: cannot open framebuffer device.\n");
        return(1);
    }

    // Get variable screen information
    if (ioctl(framebuffer_fd, FBIOGET_VSCREENINFO, &vinfo)) {
        printf("Error reading variable information.\n");
    }

    // Get fixed screen information
    if (ioctl(framebuffer_fd, FBIOGET_FSCREENINFO, &finfo)) {
        printf("Error reading fixed information.\n");
    }
    
    // Get information about Framebuffer
    get_fb_info(&vinfo,&finfo);
    parse_colormap();
REFACTOR THIS PART OF CODE
    // Alloc colormap
    cmap.red = malloc(16 * sizeof(uint16_t));
    cmap.green = malloc(16 * sizeof(uint16_t));
    cmap.blue = malloc(16 * sizeof(uint16_t));
    cmap.transp = 0;
    cmap.start = 0;
    cmap.len = 16;

    // Get color map
    if(ioctl(framebuffer_fd,FBIOGETCMAP,&cmap) == -1) 
        printf("Error FBIOGETCMAP %s\n",strerror(errno));

   for(int i=0; i < cmap.len; i++)
        printf("\tcolor%d",i);
    printf("\n");

    show_color_info(cmap.red,cmap.len,"RED");
    show_color_info(cmap.green,cmap.len,"GREEN");
    show_color_info(cmap.blue,cmap.len,"BLUE"); 


    unsigned short r[256];
    unsigned short b[256];
    unsigned short g[256];

    for(int i = 0 ; i < cmap.len; i++) {
        r[i] = default_r16[i] << 8;
        g[i] = default_g16[i] << 8;
        b[i] = default_b16[i] << 8;
    }
    
    // Set colormap
    cmap.start = 0;
    cmap.len = 16;
    cmap.red = r ;
    cmap.green = g;
    cmap.blue = b;
    cmap.transp = 0; 
    
    if(ioctl(framebuffer_fd,FBIOPUTCMAP,&cmap) == -1) 
	    printf("Error FBIOPUTCMAP %s\n",strerror(errno));

    size_t data_size = vinfo.xres * vinfo.yres * (vinfo.bits_per_pixel /8);
    char *data = mmap(0, data_size,PROT_READ | PROT_WRITE,
            MAP_SHARED,framebuffer_fd, (off_t) 0);

    // Draw 32x32 for each color
    for(int x = 100; x < 300; x++) {
        for(int y = 100; y < 164; y++) {
           int offset = (x + vinfo.xoffset) * (vinfo.bits_per_pixel /8) +
                (y + vinfo.yoffset) * finfo.line_length;
           
        data[offset] = 1;
        }
    }

    // close fb file    
    close(framebuffer_fd);

    return 0;
  
}
