// Basic termios that gets terminal 
// informations and print on stdout
// stty -a
// gcc -g -Wall -std=c11 termios_info.c -o ~/EXEC/TERMIOS_INFO

#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <unistd.h> //isatty STDIN_FILENO
#include <sys/ioctl.h>

#define STDIN_FILENO 0

void binary_fmt(int n) {
	int mask = 0x80;
	while(mask) {
		printf("%d", (n&mask) ? 1 : 0);
		mask = mask >> 1;
	}
	printf("\n");
}

int main() {
	
	struct termios term;
	struct winsize ws;

	int fd = 0;

	if(isatty(STDIN_FILENO))
		fd = STDIN_FILENO;

	tcgetattr(fd,&term);
	printf("INPUT MODE(c_iflag): "); 
	binary_fmt(term.c_iflag);
	printf("OUTPUT MODE(c_oflag): ");
	binary_fmt(term.c_oflag);
	printf("CONTROL MODE(c_cflag): ");
	binary_fmt(term.c_cflag);
	printf("LOCAL MODE(c_lflag): ");
	binary_fmt(term.c_lflag);
	printf("LINE DISCIPLINE(c_line): ");
	binary_fmt(term.c_line);

	// GET ROWS AND COLS ABOUT TERMINAL
	ioctl(fd,TIOCGWINSZ,&ws);
	printf("Rows: %d , COLS : %d",ws.ws_row,ws.ws_col);
	return 0; 
}
