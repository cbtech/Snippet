/*
 * Simple colorpalette 
 *
 *  gcc -ggdb3 -Wall colorpalette.c -o ~/EXEC/COLORPALETTE
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <errno.h>

struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;
struct fb_cmap cmap;


void get_fb_info(struct fb_var_screeninfo *vinfo,struct fb_fix_screeninfo *finfo) {
   // Change variable info
    vinfo->bits_per_pixel = 8;
    vinfo->grayscale = 0;
    finfo->visual = FB_VISUAL_TRUECOLOR;
    printf("xres: %d \t yres: %d \tbpp: %d\n", 
            vinfo->xres, vinfo->yres, 
            vinfo->bits_per_pixel );
    printf("Visual info: %d\n",finfo->visual);
    printf("vinfo.vmode is: %d\n",vinfo->vmode);
    printf("vinfo.red.offset=0x%x\n",vinfo->red.offset);
    printf("vinfo.red.length=0x%x\n",vinfo->red.length);
    printf("vinfo.green.offset=0x%x\n",vinfo->green.offset);
    printf("vinfo.green.length=0x%x\n",vinfo->green.length);
    printf("vinfo.blue.offset=0x%x\n",vinfo->blue.offset);
    printf("vinfo.blue.length=0x%x\n",vinfo->blue.length);
    printf("vinfo.transp.offset=0x%x\n",vinfo->transp.offset);
    printf("vinfo.transp.length=0x%x\n",vinfo->transp.length);
    
}

const uint32_t colormap[16] = { 
	0x000000, 0xAA0000, 0x00AA00, 0xAA5500, 0x0000AA, 0xAA00AA, 0x00AAAA, 0xAAAAAA/* 0xAAAAAA */,
	0x555555, 0xFF5555, 0x55FF55, 0xFFFF55, 0x5555FF, 0xFF55FF, 0x55FFFF, 0xDFDFDF
};

void fill_rect(char* fb_buffer, int x, int y, int w, int h,int color) {
	// offset = 0/4/8/12... create just
	// gap of size int for each pixel
    for(int x = 100;x < 300; x++) {
        for(int y = 100;y < 300; y++) {

            int offset = (x + y * vinfo.xres) * 
	            	vinfo.bits_per_pixel / 8; 
            // ALSO WORK
            /*         int offset  = (x + vinfo.xoffset) * fb_bytes + //Get the offset of X on that line
                              (y + vinfo.yoffset) * finfo.line_length; //Get the begining of line y in memory 
            */
 
            int c = colormap[0];

            unsigned char r = 170;
            unsigned char g = 85;
            unsigned char b = 0;
         
            fb_buffer[offset + 0] = b;
            fb_buffer[offset + 1] = g;
            fb_buffer[offset + 2] = r;
   
        }
    }

// ------------------------------------------------------
// fb_buffer schema Just 4 Bits 4x4 = 16
// 	   4 Bits       4 Bits             4 Bits 
//  	0 | 1 | 2     3 | 4 | 5		6 | 7 | 8
//  	r   g   b     r   g   b		r   g   b ...
// -----------------------------------------------------
	
}

int main(int argc, char* argv[])
{
    int framebuffer_fd = 0;
    int fb_size = 0;
    char *fb_buffer = NULL;
    unsigned char* cmap;

    // Open the file for reading and writing
    framebuffer_fd = open("/dev/fb0", O_RDWR);
    if (framebuffer_fd == -1) {
        printf("Error: cannot open framebuffer device.\n");
        return(1);
    }

    // Get variable screen information
    if (ioctl(framebuffer_fd, FBIOGET_VSCREENINFO, &vinfo)) {
        printf("Error reading variable information.\n");
    }

    // Get fixed screen information
    if (ioctl(framebuffer_fd, FBIOGET_FSCREENINFO, &finfo)) {
        printf("Error reading fixed information.\n");
    }

    // Get color map
    if(ioctl(framebuffer_fd,GIO_CMAP,cmap) == -1) 
        printf("Error reading CMAP\n");

    fb_size = vinfo.xres * vinfo.yres * (vinfo.bits_per_pixel / 8);
    fb_buffer = mmap(0, fb_size,PROT_READ | PROT_WRITE, MAP_SHARED,
		    framebuffer_fd, 0);

    int x,y;
    for(y = 0 ; y < vinfo.yres / 2 ;y++) {
	    for(x=0 ;x < vinfo.xres ;x++ ) {
		fill_rect(fb_buffer,x,y,100,100,56);
	    }
    }
    
    // Get information about Framebuffer
    get_fb_info(&vinfo,&finfo);

    // close fb file    
    close(framebuffer_fd);

    return 0;
  
}
