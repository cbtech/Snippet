#include <stdio.h>
#include <stdlib.h>

struct linked_list{
	char *data;
	struct linked_list *next;
};

struct linked_list* insert_back(struct linked_list *node, 
		const char *data) {
		
	struct linked_list *tmp;
	tmp = malloc(sizeof(struct linked_list));
	tmp->data = data;
	tmp->next = node;

	*node = *tmp;

	return tmp;
}

struct linked_list* insert_front(struct linked_list* node,
		const char *data) {
	struct linked_list *tmp;
	tmp = malloc(sizeof(struct linked_list));
	tmp->data = data;
	tmp->next = NULL;

	*node = *tmp;
	node->next = tmp;

	return tmp;
}

void display_node(struct linked_list *node,
		struct linked_list *head) {
	int i;
	struct linked_list *tmp = head;

	for(i = 0 ; tmp != node ; tmp = tmp->next,i++) {
		printf("Elem: %d, Data: %s\n",i,tmp->data);
	}
}

int main(int argc,char *argv[]) {
	struct linked_list *node = NULL;
	node = malloc(sizeof(struct linked_list));

	struct linked_list *head = node;

	// USE ARRAY TO STORE ALL ADRESSES
	
	node = insert_front(node,"Elem1");
	node = insert_front(node,"Elem2");
	node = insert_back(node,"ElemX");
	
	display_node(node,head);
}
