// Linked list 
#include <stdio.h>
#include <stdlib.h>

struct linked_list {
	char *data;
	struct linked_list* next;
};

struct linked_list *create_node(struct linked_list *node, char *str) {
	struct linked_list *tmp = malloc(sizeof(struct linked_list ));
	tmp->data = str;
	tmp->next = NULL;
 
	node->data = tmp->data;
        node->next = tmp;

	free(tmp);
	return tmp;
}

void display_nodes(struct linked_list *head, struct linked_list *node) {
   int i;
   
   struct linked_list *tmp = head;
   // Head is the first element, node the last element
   for ( i = 0; tmp != node; tmp = tmp->next,i++) 
		printf("Element %d: %s\n",i,tmp->data);
}

int main() {
	struct linked_list *head,*node;
  	
	node = malloc(sizeof(struct linked_list));

	head = node;	
	node = create_node(node,"Elem1");
	node = create_node(node,"Elem2");
	node = create_node(node,"Elem3");
	node = create_node(node,"Elem4");
	node = create_node(node,"Elem5");

	display_nodes(head,node);

	free(node);
	return 0;
}

