#include <stdio.h>
#include <stdlib.h>

struct scrapper{
	char *title;
	char *id_1;
	char *id_2;
};

struct buffer {
	char *content;
	int size;
};

// V1
void allocate(struct scrapper *s,size_t sz) {
	s->title = malloc(sz * sizeof(char));
}

/* V2
void allocate(char **t,size_t sz) {
	*t = malloc(sz * sizeof(char));
}
*/

size_t _strlen(const char *str) {
	int i=0;
	for (; *str !='\0'; str++) { i++;}
	return i;
}


void extract_title(struct scrapper *s) {
	int i=0;

	char *msg = "Hello, My name is JackLemaitre. And I live Toronto."; 
	_strlen(msg);

	// V3 This works 	
	// s->title = malloc(_strlen(msg) * sizeof(char));

	// V1
	allocate(s,_strlen(msg));
	
	// V2 used with allocate(char **t,size_t sz)	
	// allocate(&s->title,_strlen(msg));

	for( ; *msg != '\0'; msg++,i++) { 
		s->title[i]= *msg;
	}
}

int main(int argc,char **argv) {
	struct scrapper s;

	extract_title(&s);
	
	printf("TITLE: %s\n",s.title);
	return 0;
}
