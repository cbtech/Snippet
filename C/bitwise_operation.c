#include <stdio.h>

#define ICANON 0000002
#define ISIG   0000001

void binary(unsigned int n)
{
	unsigned mask=0x80;
	while(mask) {
		printf("%d",(n&mask)?1:0);
		mask = mask >> 1;
	}
}

int main(int argc,char *argv[])
{
	unsigned short int c_iflag = ICANON;
	
	printf("ISIG: "); binary(ISIG);
	printf("\n");
	printf("ICANON: "); binary(ICANON);
	printf("\n");
	printf("~ ICANON: "); binary(~ICANON);
	printf("\n");
	c_iflag &= (ICANON | ISIG);
	printf("c_iflag &= :"); binary(c_iflag);
	printf("\n");

	/* OUTPUT
	 * ISIG: 00000001
	 * ICANON: 00000010
	 * ~ICANON: 1111101
	 * ciflag &= :0000010
	 */

	return 0;
}
