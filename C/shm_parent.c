#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>


#define SHM_KEY 0X1234


int main(int argc,char *argv[]) {
	// ftok to generate unique key 
	key_t key = ftok("shmfile",65);

	// shmget return an identifier in shmid
	int shmid = shmget(key,1024,0666|IPC_CREAT);

	// shmat to attach to shared memory
	char *str = (char*) shmat(shmid,(void*)0,0);

	printf("Enter name: ");
	scanf("%s",str);

	printf("Done\n");

	//detach from shared memory
	shmdt(str);
	return 0;
}
