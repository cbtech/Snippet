#include <stdio.h>
#include <fcntl.h>
#include <sys/time.h>
#include <linux/input.h> // struct input_event

// Defined in input-event-codes.h
// #define EV_KEY	0x1f
#define MAX_WIDTH 1280
#define MAX_HEIGHT 1024

#define EV_MAX	0x1f
#define EV_CNT	(EV_MAX+1)
#define EV_REL	0x02

#define BTN_LEFT 0x110

#define VALUE_RELEASED  0
#define VALUE_PRESSED  1

struct cursor_t {
	int x,y;
};

struct mouse_t {
	struct cursor_t current;
	struct cursor_t pressed,released;
};

void init_mouse(struct mouse_t *mouse)
{
	mouse->current.x = mouse->current.y = 0;
	mouse->pressed.x = mouse->pressed.y = 0;
	mouse->released.x = mouse->released.y = 0;
//	mouse->button_pressed = false;
//	mouse->button_released = false;
}

void print_mouse_state(struct mouse_t *mouse) {
	fprintf(stderr, "\033[1;1H\033[2Kcurrent(%d,%d) pressed(%d, %d) released(%d,%d)",
			mouse->current.x, mouse->current.y,
			mouse->pressed.x, mouse->pressed.y,
			mouse->released.x,mouse->released.y);
}

void cursor(struct input_event *ie,struct mouse_t *mouse) {
	if(ie->code == REL_X)
		mouse->current.x += ie->value;
	if(mouse->current.x < 0)
		mouse->current.x = 0;
	else if(mouse->current.x >= MAX_WIDTH)
		mouse->current.x = MAX_WIDTH - 1;

	if(ie->code == REL_Y)
		mouse->current.y += ie->value;
	if(mouse->current.y < 0)
		mouse->current.y = 0;
	else if(mouse->current.y >= MAX_HEIGHT)
		mouse->current.y = MAX_HEIGHT -1;
}

void button(struct input_event *ie,struct mouse_t *mouse) {
	if(ie->code != BTN_LEFT)
		return;
	if(ie->value == VALUE_PRESSED)
		mouse->pressed = mouse->current;
	if(ie->value == VALUE_RELEASED)
		mouse->released = mouse->current;
}

void (*event_handler[EV_CNT])(struct input_event *ie,struct mouse_t *mouse) = {
	[EV_REL] = cursor,
	[EV_KEY] = button,
	[EV_MAX] = NULL,
};

int main(int argc, char *argv[])
{
	struct input_event ev;
	struct mouse_t mouse;
	init_mouse(&mouse);

	int fd;
	// dev/input/event10 → pad wacom
	// /dev/input/event5 → mouse
	if ((fd = open("/dev/input/event5", O_RDONLY)) == -1) {
		printf("Erreur reading /dev/input/event5\n");
	}

	while(read(fd, &ev,sizeof(struct input_event))) {
		//print_event(ev);
		print_mouse_state(&mouse);
		if(event_handler[ev.type])
			event_handler[ev.type](&ev,&mouse);
	}

	close(fd);

	return 0;
}

