/*
 In this example, we first define a few constants: TEMPO (in beats per minute), BEATS_PER_BAR (the number of beats in a bar), and MICROSECONDS_PER_MINUTE (the number of microseconds in a minute). We then define a loop that will run indefinitely.

Inside the loop, we calculate the duration of a beat in microseconds based on the tempo, using the formula microseconds_per_beat = MICROSECONDS_PER_MINUTE / TEMPO. We then play a click sound or flash a light to indicate the beat, increment the beat counter, and wait for the duration of a beat using the usleep() function.

The loop will continue to play beats indefinitely, incrementing the beat counter and wrapping around at the end of each bar.

Note that this is just a simple example, and in practice, you would likely want to add more features, such as the ability to set the tempo and the ability to play different sounds or flash different lights for different beats.
 */

#include <stdio.h>
#include <unistd.h>

#define TEMPO 120 // beats per minute
#define BEATS_PER_BAR 4
#define MICROSECONDS_PER_MINUTE 60000000

int main() {
    int beat = 1;

    while (1) {
        // Calculate the duration of a beat in microseconds based on the tempo
        int microseconds_per_beat = MICROSECONDS_PER_MINUTE / TEMPO;

        // Play a click sound or flash a light to indicate the beat
        printf("Beat %d\n", beat);

        // Increment the beat counter and wrap around at the end of the bar
        beat = (beat % BEATS_PER_BAR) + 1;

        // Wait for the duration of a beat before playing the next one
        usleep(microseconds_per_beat);
    }

    return 0;
}

