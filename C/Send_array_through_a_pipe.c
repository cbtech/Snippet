//
//  cc -g -Wall -w Send_array_through_a_pipe.c -o Array_pipe
//
//


#include <stdio.h>
#include <unistd.h>
#include <time.h>


// [ EXPLANATION ]
//	 2 processes (Parent and Child)
//	 1) Child Process should generate random 
//    	    numbers and send them to the parent.
//    	    (never read from the child)
//       2) Parent is going to sum all the numbers 
//          and print the result(never write with parent).

int main(int argc, char *argv[])
{
	int fd[2];
	
	// pipe create pipe(unidirectionnal data channel
	//   that can be used for interprocess communication
	if (pipe(fd) == -1) fprintf(stderr,"Cannot create pipe\n");

	// fork create child process
	int pid = fork();

	if(pid == -1) fprintf(stderr, "Cannot create child process\n");
	
	if(pid == 0) {
	   // We are in the child process
	   // Close reading
	   close(fd[0]);
	   int n;
	   int arr[10];
	   srand(time(NULL));
	   n = rand() % 10 + 1;

	   printf("Generated: ");
	   for(int i = 0; i < n; i++) {
		   arr[i] = rand() % 11;
		   printf("%d ", arr[i]);
           }
	   printf("\n"); 
	   // Writing the number of element to tell
	   // the size to the parent 
	   if(write(fd[1], &n, sizeof(int)) < 0)
		   fprintf(stderr,"Cannot write the number of elements\n");

	   printf("Sent n = %d\n", n);

	   if(write(fd[1], arr, sizeof(int) * n) < 0) 
		   fprintf(stderr,"Cannot write array to the pipe\n");
	   
	   // Close writing
	   printf("Sent array\n");
	   close(fd[1]);
	   } 
	else {
	  // Parent Process
	  close(fd[1]);
	  int arr[10];
	  int n, i, sum = 0;
	  if (read(fd[0], &n , sizeof(int)) < 0 )
	  	fprintf(stderr, "Error, cannot read number of elements\n");
	  
	  printf("Received n = %d\n",n);
	  
	  if (read(fd[0], arr, sizeof(int) * 10) < 0)
	  	fprintf(stderr,"Error, cannot read array\n");

	  close(fd[0]);
	  for (int i = 0; i < n ; i++) 
		  sum += arr[i];

	  printf("Result is: %d\n", sum);
	  wait(NULL);
	}

	return 0;
}
