#include <stdio.h>
#include <stdlib.h>

int main()  {
	int *arr = NULL;
	int i, j , nb_elems;

	printf("Enter the number of elements: ");
	scanf("%d",&nb_elems);
	arr = malloc(nb_elems * sizeof(int));

	printf("Enter elements:\n ");
	// ONESHOT
	for(i = 0; i < nb_elems; i++)
		scanf("%d",&arr[i]);
	
	for(i = 0; i < nb_elems; i++) {
 		for(j = 1; j < nb_elems; j++) {
			if( arr[j-1] > arr[j]) {
				arr[j] = arr[j] ^ arr[j-1];
				arr[j-1] = arr[j] ^ arr[j-1];
				arr[j] 	= arr[j] ^ arr[j-1];
			}
		}
	}
	printf("Ascending order: ");
	for(i = 0; i < nb_elems; i++)
		printf("%d\t",arr[i]);
	printf("\n");
	free(arr);
	return 0;
}
