// Reverse a string using XOR or PTR

//  gcc -g -Wall -std=c11 -DXOR reverse_str.c
//             -o ~/EXEC/RV_STR_XOR
//
//  gcc -g -Wall -std=c11 -DPTR revserse_str.c 
//  	       -o  ~/EXEC/RV_STR_PTR
//

#include <stdio.h>
#include <string.h>

#define STR_LEN  50
int main() {
	char str[STR_LEN];
	char *i,*j;

	printf("Enter a String: ");
	fgets(str,STR_LEN,stdin);

#if defined(XOR)
	// String: Hello 
	for(i = str,j = str + strlen(str) -1;j>i; ++i,--j) {
		//  *i is the first char of str (H) 
		*i = *i^*j;
		// ^ corresponding to XOR Bitwize
		*j = *j^*i;
		*i = *i^*j;
	}

	printf("The reversed string: %s\n",str);

#elif(PTR)
	int idx;
	char *p = str;
        p += strlen(str);
        printf("The reversed string: ");
		for(i = strlen(str); idx >= 0;idx--) {
			printf("%c",*p--);
		}
	}

#endif
}


