#include <stdio.h>
#include <fcntl.h> //O_WRONLY/O_NONBLOCK/
#include <unistd.h> //read/write/close
#include <sys/io.h>
#include <string.h>
#include <linux/uinput.h> // usetup 
#include <linux/input.h> //input_event struct

void emit(int fd, int type, int code, int val)
{
	struct input_event ie;
	  
	ie.type = type;
	ie.code = code;
	ie.value = val;

	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	write(fd, &ie, sizeof(ie));
}

void event_BTN1(struct uinput_setup ue)
{

	int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	int CTRL_KEY=0x1d;
	//0x2d for X key
	int TEST_KEY=0x2d; 

	ioctl(fd,UI_SET_EVBIT,EV_KEY);
	ioctl(fd, UI_DEV_SETUP, &ue);
	// Set bit for CTRL
	ioctl(fd,UI_SET_KEYBIT, CTRL_KEY);
	ioctl(fd,UI_SET_KEYBIT,TEST_KEY);
	ioctl(fd,UI_DEV_CREATE);

	sleep(1);

	// SEND CTRL + x EVENTS
	emit(fd,EV_KEY,CTRL_KEY, 1);
	emit(fd,EV_SYN, SYN_REPORT,0);
	
	// Send X event
	emit(fd,EV_KEY, TEST_KEY,1);
	emit(fd,EV_SYN,SYN_REPORT,0);

	// Unleash CTRL
	emit(fd,EV_KEY, CTRL_KEY, 0);
	emit(fd,EV_SYN, SYN_REPORT, 0);
	
	// Unleash X
	emit(fd,EV_KEY,TEST_KEY,0);
	emit(fd,EV_SYN,SYN_REPORT,0);

	sleep(1);

	// Destroy the event
	ioctl(fd, UI_DEV_DESTROY);
	close(fd);

}

void pressBTN() { 
	struct uinput_setup usetup;
	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_USB;
	usetup.id.vendor = 0x1342;
	usetup.id.product = 0x9876;
	strcpy(usetup.name, "Virtual Keyboard");
 
	printf("[pressBTN] executed\n");
	event_BTN1(usetup); 
}

void (*event_handler[EV_CNT])(struct input_event *ie) ={
	[EV_KEY]=pressBTN,
};

int main(int argc,char *argv[]) 
{
	struct input_event ev;
	int fd;
	if((fd = open("/dev/input/event10",O_RDONLY)) == -1) 
		fprintf(stderr, "Error opening input device\n");

	while(read(fd,&ev,sizeof(struct input_event))) {
		if(event_handler[ev.type])
			event_handler[ev.type] (&ev);
	}

		return 0;
}

