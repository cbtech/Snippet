// Simple Media Player using SDL2
// gcc -ggdb3  -Wall -o0 sdlamp.c `sdl2-config --cflags --libs` -o SDLAMP 
#include "SDL.h"

// OpenAudioDevice Declaration in SDL_audio.h line 669
static SDL_AudioDeviceID audio_device = 0;
 
int main(int argc,char **argv) {
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

	SDL_AudioSpec wavspec;
	Uint8 *wavbuf = NULL;
	Uint32 wavlen = 0;

	if(SDL_LoadWAV("music.wav",&wavspec,&wavbuf,&wavlen) == NULL) {
		fprintf(stderr, "Ohoh, Couldn't load WAV file! %s\n",SDL_GetError());
		SDL_Quit();
		return 1;
	}

	// SDL_Audio.h line 669
	audio_device = SDL_OpenAudioDevice(NULL,0,&wavspec, NULL, 0);
	
	SDL_QueueAudio(audio_device , wavbuf, wavlen);
	SDL_FreeWAV(wavbuf);
	
	SDL_PauseAudioDevice(audio_device,0);
	SDL_Delay(30000);

	SDL_CloseAudioDevice(audio_device);
	SDL_Quit();
	return 0;
}
