// gcc -g -Wall -w Map.c -o Map -lSDL2 -lSDL2_image

#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "config.h"
#include "resources/maps.h"

typedef struct {
	SDL_Window* Window;
	SDL_Renderer *renderer;
} Game;

typedef struct {
	int id;
	SDL_Rect src;
	SDL_Rect dst;
} Tile;

int initGame(Game* g) {
	if(SDL_Init(SDL_INIT_VIDEO) < 0) return 1;
	
	IMG_Init(IMG_INIT_PNG);

	// CREATE SDL WINDOW
	g->Window = SDL_CreateWindow("SDLMap", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_OPENGL);
	
	if(g->Window == NULL) 
		printf("Could not create a window: %s", SDL_GetError());

	// CREATE RENDERER TO SHOW CONTENTS
	g->renderer = SDL_CreateRenderer(g->Window,-1,
																	SDL_RENDERER_ACCELERATED);
	if(g->renderer == NULL ) 
		printf("Could not create renderer: %s", SDL_GetError());				
}

void freeGame(Game* g) {
	SDL_DestroyRenderer(g->renderer);
	SDL_DestroyWindow(g->Window);
	SDL_Quit();
}

void readMap(Game* g)
{
	printf("%d\n",map1[0]);
}

void createMap(Game* g) {
	Tile *tile[NB_TILE_WIDTH *NB_TILE_HEIGHT];	


	for (int y = 0 ; y < NB_TILE_HEIGHT; y++) {
			for (int x = 0; x < NB_TILE_WIDTH; x++) {
				tile[y * NB_TILE_WIDTH + x] = malloc(sizeof(tile));
				tile[y * NB_TILE_WIDTH + x]->src.x = 0;
				tile[y * NB_TILE_WIDTH + x]->src.y = 32;
				tile[y * NB_TILE_WIDTH + x]->src.w = TILE_SIZE;
				tile[y * NB_TILE_WIDTH + x]->src.h = TILE_SIZE;

				tile[y * NB_TILE_WIDTH + x]->dst.x = x * TILE_SIZE;
				tile[y * NB_TILE_WIDTH + x]->dst.y = y * TILE_SIZE;
				tile[y * NB_TILE_WIDTH + x]->dst.w = TILE_SIZE;
				tile[y * NB_TILE_WIDTH + x]->dst.h = TILE_SIZE;
		  }
	}
	drawMap(&tile,g);
}

void drawMap(Tile **tile,Game* g) {
	// LOAD TILESET
	SDL_Surface* tileset = IMG_Load("resources/levelgridless.png");

	if(tileset == NULL ) 
			printf("Unable to load image: %s \n", SDL_GetError());
	
	// INITIALIZE TEXTURE
	SDL_Texture* texture = NULL;
	texture = SDL_CreateTextureFromSurface(g->renderer, tileset);
	// FILL THE SCREEN WITH WHITE AND CLEAR
	SDL_SetRenderDrawColor(g->renderer, 255, 255, 255, 255);
	SDL_RenderClear(g->renderer);

	// SHOW THE MAP
	for(int y = 0; y < NB_TILE_HEIGHT; y++) {
		for(int x = 0; x < NB_TILE_WIDTH; x++) {
			  SDL_RenderCopy(g->renderer,texture,
				&tile[y * NB_TILE_WIDTH + x]->src,
				&tile[y * NB_TILE_HEIGHT + x]->dst); 
		}
		}
		SDL_RenderPresent(g->renderer);

		// FREE THE SURFACE
		SDL_FreeSurface(tileset);
		SDL_DestroyTexture(texture);
}

int main(int argc, char *argv[])
{
	Game game;
	
	initGame(&game);
	
	readMap(&game);	
	createMap(&game);

	while(true)
	{
		SDL_Event event;
		if(SDL_PollEvent(&event))
		{
			if(event.type == SDL_KEYDOWN)
			{
				if(event.key.keysym.sym == SDLK_ESCAPE)
					break;
			}
			if(event.type == SDL_QUIT) break;
		}
	}

	freeGame(&game);

	return 0;
}

