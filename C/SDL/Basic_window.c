// gcc -g -Wall -w Map.c -o Map -lSDL2

#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

int main(int argc, char *argv[])
{
	
	SDL_Window* gWindow = SDL_CreateWindow("SDL MAP Editor",0,
                        	   0,100,100,SDL_WINDOW_OPENGL);
	if(gWindow == NULL ) SDL_Log("Could not create a window: %s", SDL_GetError());
	
	SDL_Renderer *renderer = SDL_CreateRenderer(gWindow,-1,SDL_RENDERER_ACCELERATED);
	if(renderer == NULL ) SDL_Log("Could not create renderer: %s", SDL_GetError());												
	
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderClear(renderer);
	  SDL_RenderPresent(renderer);

	while(true)
	{
		SDL_Event event;
		if(SDL_PollEvent(&event))
		{
			if(event.type == SDL_KEYDOWN)
			{
				if(event.key.keysym.sym == SDLK_ESCAPE)
					break;
			}
			if(event.type == SDL_QUIT)
			{
				break;
			}
		}
	}

	SDL_DestroyWindow(gWindow);
	SDL_Quit();

	return 0;
}

