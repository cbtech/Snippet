#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_LEN 256
int main() {
	char str[STR_LEN];
	char *tmp = NULL;
	int i;

	printf("Enter the string: ");
	fgets(str,STR_LEN,stdin);
	tmp = malloc((strlen(str) + 1) * sizeof(char));
	for(int i = 0; i < strlen(str); i++ ) {
			tmp[i] = str[i];
	}

	printf("The copied string is: %s \n",tmp);
	free(tmp);
	return 0;
}
