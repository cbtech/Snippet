#include <stdio.h>
#include <stdlib.h>

enum VALID {FALSE, TRUE};

typedef struct {
	int valid;
	int *data;
	char *name;
	size_t size;
} MyObject;

int main(int argc, char **argv[])
{
	int *tmp = NULL;
	MyObject *my1 = malloc(sizeof(MyObject));

	my1->name="Jack";
	my1->valid = TRUE;
	my1->data = tmp;
	my1->size = sizeof tmp;

	printf("Name: %s\n",my1->name);
	printf("VALID: %d",my1->valid);
	free(my1);
	exit(EXIT_SUCCESS);
	return 0;
}
