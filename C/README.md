## GENERAL 
[alloc.c](Simple allocation code using malloc)
[bit_manipulation.c](Bit manipulation AND&/ XOR| / XOR^)
[bitwise_operation.c](Bitwise operation on Terminal Flag)
[buffer.c](Buffer management with allocation)
[chartoint.c](Convert char to int)
[copystr.c](Copy string using fgets)
[execl.c](execl Example)
[FIFO_Read.c](R/W FIFO file using read/write)
[fread_fwrite.c](Reading file using  fread/fwrite)
[HTMLParser.c](HTML Parser in C)
[read_file.c](R/W file using fscanf/fprintf)
[bintodec.c](Convert binary to decimal and dec to bin)
[Read_array_of_struct.c] (Read values stored in external 
		          file and in array of struct)
[struct_alloc.c] (Allocate struct with `char *` inside)
[struct_allocation.c] (Allocate struct with different type 
		       inside)
[loop.c](basic for loop)
[swap_num.c](Swap int using xor/pointers)
[vowel.c](Check vowel in string)
[up2low.c](Convert Uppercase to Lowercase)
[removews.c](Remove Whitespace in str) 
[palindrome.c](Check palindrome in string)
[arrange_nb.c](Arrange Number in ascending order)
[reverse_str.c](Reverse string/XOR)
[reverse_array.c](Reverse array using XOR)
[linkedlist.c](Linked list)

## LINUX PROGRAMMING INTERFACE
[LPI/cp.c] (Copy file using system call)
[LPI/fseek.c] (Fseek example)

## CURL 
[curl_capturing_group.c](Example of capturingGroup)
[curl_grabwebpage.c](Grab webpage using CURL)

## FRAMEBUFFER / PSEUDO TERMINAL /TERMIOS
[FB_PTY/termios_info.c](Termios flag info and mode)
[FB_PTY/terminal_key.c](Termios Keycode Press/Rel)
[FB_PTY/SimpleFramebuffer.c](Fill background framebuffer)
[FB_PTY/pty.c](Simple pseudoterminal example)


