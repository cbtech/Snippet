/*
 * WLHelloWorld.c
 * Wayland Hello world 
 * cc -g -Wall -w WL_Helloworld.c 
 * xdg-shell-client-protocol.c -L/usr/local/lib 
 * -lwayland-client -o WLHelloworld
 *	
 * xdg-shell-client-protocol.h:
 *	wayland-scanner client-header 
 *	  </usrshare/wayland-protocols/stable/xdg-shell/xdg-shell.xml>
 *  	  xdg-shell-clent-protocol.h
 *
 * xdg-shell-client-protocol.c:
 *	wayland-scanner private-code 
 *	  </usrshare/wayland-protocols/stable/xdg-shell/xdg-shell.xml>
 *	  xdg-shell-clent-protocol.c
 *
 * xdg-shell-client-protocol for
 *   xdg_surface_interface / xdg_surface_get_toplevel / xdg_wm_base_interface
*/

#define true 1
#define false 0

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/mman.h>
#include<wayland-client.h>
#include<time.h>
#include<wayland-client-protocol.h>

#include "cat.h"
#include <linux/input-event-codes.h>
#include "xdg-shell-client-protocol.h"

static const int width = 128;
static const int height = 128;

static int running = true;
static struct wl_shm *shm = NULL;
static struct wl_compositor *compositor = NULL;
static struct xdg_wm_base *xdg_wm_base = NULL;

static void *shm_data = NULL;
static struct wl_surface *surface = NULL;
static struct xdg_toplevel *xdg_toplevel = NULL;

static const struct wl_seat_listener seat_listener;
static const struct wl_pointer_listener pointer_listener;

// ----------------[ SHARED MEMORY SHM ] --------------
static void randname(char *buf)
{
	// Create random name
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME,&ts);
	long r = ts.tv_nsec;
	for(int i = 0; i < 6; i++)
	{
		// AND bitwise r&15 means range between 0 and 15 inclusive.
		// r&16 will be either 0 or 16 *  32 added to A gives a.
		// produce character between 'A'-'O' or 'a' -'o'
		buf[i] = 'A'+(r&15)+(r&16)*2;
		// bitshift to the right by 5
		r >>= 5;
	}
}

static int anonymous_shm_open(void)
{
	char name[] = "/hello_wayland-XXXXXX";
	int retries = 100;

	do {
		randname(name + strlen(name) -6);
		--retries;

		//shm_open guaranteed that O_CLOEXEC is set
		int fd = shm_open(name,O_RDWR | O_CREAT | O_EXCL, 0600);
		if(fd >= 0) {
			shm_unlink(name);
			return fd;
		}
	}
	while(retries > 0 && errno == EEXIST);
	return -1;

}

int create_shm_file(off_t size) {
	int fd = anonymous_shm_open();
	if (fd < 0)
		return fd;

	if(ftruncate(fd,size) < 0) {
		close(fd);
		return -1;
	}
	return fd;
}

// ---------- [ FUNCS FOR WL_REGISTRY LISTENER ] -------
static void handle_global(void *data,struct wl_registry *registry,
		uint32_t name,const char *interface,uint32_t version)
{
	if(strcmp(interface,wl_shm_interface.name) == 0) {
		shm = wl_registry_bind(registry,name,&wl_shm_interface,1);
	}
	else if(strcmp(interface,wl_seat_interface.name) == 0) {
		struct wl_seat *seat = wl_registry_bind(registry, name,&wl_seat_interface,1);
		wl_seat_add_listener(seat,&seat_listener,NULL);
	}
	else if(strcmp(interface,wl_compositor_interface.name) == 0) {
		compositor = wl_registry_bind(registry,name,&wl_compositor_interface, 1);
	}
	else if(strcmp(interface,xdg_wm_base_interface.name) == 0) {
		xdg_wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface , 1);
	}
}

static void handle_global_remove(void *data,struct wl_registry *registry,uint32_t name)
{
	// who cares
}

static void seat_handle_capabilities(void *data, struct wl_seat *seat,
		uint32_t capabilities) {
	if(capabilities & WL_SEAT_CAPABILITY_POINTER) {
		struct wl_pointer *pointer = wl_seat_get_pointer(seat);
		wl_pointer_add_listener(pointer,&pointer_listener,seat);
	}
}

// ---------- [ FUNCS FOR WL_POINTER / WL_SEAT LISTENER ] -------
static void noop() {
	// EMPTY
}

static void pointer_handle_button(void *data, struct wl_pointer *pointer,
		uint32_t serial, uint32_t time, 
		uint32_t button, uint32_t state) {
	struct wl_seat *seat = data;

	/* xdg_toplevel_move:
	 *  This request must be used in reponse to some sort 
	 *  of user action like a button press
	 * - This passed serial is used to determine
	 *  the type of interactive move(touch/pointer/etc)
	 *
	 *  The server may ignore move requests depending
	 *  on the state of the surface(Fullscreen,Maximized)
	*/
	if(button == BTN_LEFT && state == WL_POINTER_BUTTON_STATE_PRESSED) {
		xdg_toplevel_move(xdg_toplevel,seat,serial);
	}
}

// ------------------- [ XDG LISTENER ] --------------
static void xdg_surface_handle_configure(void* data,
		struct xdg_surface *xdg_surface,uint32_t serial) 
{
	xdg_surface_ack_configure(xdg_surface,serial);
	wl_surface_commit(surface);
}

static void xdg_toplevel_handle_close(void *data,struct xdg_toplevel *xdg_toplevel)
{
	running = false;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_handle_configure,
};

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = noop,
	.close = xdg_toplevel_handle_close,
};

// ------------------- [ LISTENER ] ------------------
static const struct wl_pointer_listener pointer_listener = {
	.enter = noop,
	.leave = noop,
	.motion = noop,
	.button = pointer_handle_button,
	.axis = noop,
};

static const struct wl_seat_listener seat_listener = {
	.capabilities = seat_handle_capabilities,
};

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = handle_global_remove,
};

// -------------------- [ BUFFER ] ------------------
static struct wl_buffer *create_buffer() {
	int stride = width * 4;
	int size = stride * height;

	int fd = create_shm_file(size);
	if (fd < 0) {
		fprintf(stderr,"Creating buffer file for %d",
				"B failed: %m\n",size);
		return NULL;
	}

	shm_data = mmap(NULL,size, PROT_READ | PROT_WRITE, 
			MAP_SHARED,fd,0);
	
	if(shm_data == MAP_FAILED) {
		fprintf(stderr,"mmap failed: %m\n");
		close(fd);
		return NULL;
	}

	struct wl_shm_pool *pool = wl_shm_create_pool(shm,fd,size);
	struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool,0,width,height,
			stride,WL_SHM_FORMAT_ARGB8888);
	wl_shm_pool_destroy(pool);

	// MagickImage is from cat.h
	memcpy(shm_data,MagickImage,size);
	return buffer;
}

// -------------------- [ MAIN ] -------------------- 
int main()
{
	struct wl_display *display = wl_display_connect(NULL);
	
	if(display == NULL) {
		fprintf(stderr,"Failed to create display\n");
		return EXIT_FAILURE;
	}

	struct wl_registry *registry = wl_display_get_registry(display);

	
	// get_registry (request to create a registry object 
	// that allows the client to list and bind the global
	// object 

	wl_registry_add_listener(registry,&registry_listener,NULL);
	wl_display_roundtrip(display);

	if(shm == NULL | compositor == NULL || xdg_wm_base == NULL) {
		fprintf(stderr,"no WL_shm,WL_compositor or xdg_wm_base support\n");
		return EXIT_FAILURE;
	}

	struct wl_buffer *buffer = create_buffer();
	if(buffer == NULL) {
		return EXIT_FAILURE;
	}

	surface = wl_compositor_create_surface(compositor);
	struct xdg_surface *xdg_surface = 
		xdg_wm_base_get_xdg_surface(xdg_wm_base,surface); 
	xdg_toplevel = xdg_surface_get_toplevel(xdg_surface);

	xdg_surface_add_listener(xdg_surface,&xdg_surface_listener,NULL);
	xdg_toplevel_add_listener(xdg_toplevel,&xdg_toplevel_listener,NULL);

	wl_surface_commit(surface);
	wl_display_roundtrip(display);

	wl_surface_attach(surface, buffer, 0,0);
	wl_surface_commit(surface);

	while (wl_display_dispatch(display) != -1 && running)
	{
		// This space intentionnaly left blank
	}

	xdg_toplevel_destroy(xdg_toplevel);
	xdg_surface_destroy(xdg_surface);
	wl_surface_destroy(surface);
	wl_buffer_destroy(buffer);

	return 0;
}


