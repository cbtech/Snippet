#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

// Spawn SHM_KEY or generate key with ftok process
#define SHM_KEY 0X1234

int main() {

	key_t key = ftok("shmfile",65);
	int shmid = shmget(key,1024,0666|IPC_CREAT);

	char *str = (char*) shmat(shmid,(void*)0,0);
 	printf("String: %s\n",str);

	// detach from shared memory
	shmdt(str);
	return 0;
}
