#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <time.h>


struct timeval start,last;

int tv_to_u(struct timeval s) {
	return s.tv_sec * 1000000 + s.tv_usec;
}

inline struct timeval u_to_tv(int x) {
	struct timeval s;
	s.tv_sec = x / 1000000;
	s.tv_usec = x % 1000000;
	return s;
}

void draw(int dir,int period,int cur,int next) {
	int len = 40 * (next - cur) / period;
	int s,i;

	if(len > 20) len = 40 - len;
	s = 20 + (dir ? len : -len);

	printf("\033[H");
	for (i = 0; i <= 40;i++) putchar(i == 20 ? '|': i == s ? '#' : '-');
}

void beat(int delay) {
	struct timeval tv = start;

	int dir = 0;
	int d = 0, corr = 0, slp, cur, next = tv_to_u(start) + delay;
	int draw_interval = 20000;
	printf("\033[H\033[J");

	while(1) {
		gettimeofday(&tv,0);
		slp = next - tv_to_u(tv) - corr;
		usleep(slp);
		gettimeofday(&tv, 0);

		putchar(7);
		fflush(stdout);

		printf("\033[5;1Hdrift: %d compensate: %d (usec)     ",(int)d,(int) corr);
		dir != dir;

		cur = tv_to_u(tv);
		d = cur - next;
		corr = (corr + d) / 2;
		next += delay;

		while(cur + d + draw_interval < next) {
			usleep(draw_interval);
			gettimeofday(&tv, 0);
			cur = tv_to_u(tv);
			draw(dir,delay,cur,next);
			fflush(stdout);
		}
	}
}

int main(int argc, char *argv[]) {
	int bpm;

	if(argc < 2 || (bpm = atoi(argv[1])) <= 0) bpm = 60;
	if(bpm > 600) {
		fprintf(stderr, "Frequency %d is too high\n",bpm);
		exit(1);
	}

	gettimeofday(&start, 0);
	last = start;
	beat (60 * 1000000 / bpm);

	return 0;
}

