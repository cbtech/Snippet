#include <stdio.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
	int framebuffer_descriptor = open("/dev/fb0",O_RDWR);

	struct fb_var_screeninfo finfo;
	struct fb_var_screeninfo vinfo;


	ioctl(framebuffer_descriptor,FBIOGET_FSCREENINFO,&finfo);
	ioctl(framebuffer_descriptor,FBIOGET_VSCREENINFO,&vinfo);

	int fb_width = vinfo.xres;
	int fb_height = vinfo.yres;
	//fb_bpp = 32, each pixel has 4bytes in size 
	int fb_bpp = vinfo.bits_per_pixel;
	int fb_bytes = fb_bpp / 8;

	// Calculate the amount of memory it occupies
	int fb_data_size = fb_width * fb_height * fb_bytes;
	
	char *fb_data = mmap(0, fb_data_size,
			PROT_READ | PROT_WRITE, MAP_SHARED,
			framebuffer_descriptor, (off_t)0);
	
	// Read or write the mapped Screen
	// Blank the entire screen set the area to 0
	memset(fb_data, 0, fb_data_size);


	// Write a specific pixel 
	for(int x= 10; x < fb_width; x++) {
		for(int y = 10; y < fb_height; y++) {

	int offset = (x + y * fb_width) * fb_bytes;
	
	unsigned char r = 0;
	unsigned char g = 45;
	unsigned char b = 54;

	fb_data[offset + 0] = b;
	fb_data[offset + 1] = g;
	fb_data[offset + 2] = r;
		}
	}

	// Clean up the framebuffer
	munmap (fb_data, fb_data_size);
	close(framebuffer_descriptor);
}
