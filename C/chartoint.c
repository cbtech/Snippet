#include <stdio.h>

size_t get_size_array(char *arr) {
	int tmp = 0;
	for (; *arr !='\0'; arr++) {
		tmp++;
	}
	return tmp;
}

int main() {
	char arr[]={'&','"','\'','[','-','_',')','=',
		    '~','#','{','[','|','`','\\','^',
		    '@',']','}',',',';',':','!','<','>',
		    '?','.','/','>','*','$','"','%','a',
		    'b','c','d','e','f','g','h','i','j',
		    'k','l','m','n','o','p','q','r','s',
		    't','u','v','w','x','y','z','A','B',
		    'C','D','E','F','G','H','I','J','K',
		    'L','M','N','O','P','Q','R','S','T',
		    'U','V','W','X','Y','Z','1','2','3',
		    '4','5','6','7','8','9','0'
	};
	
	printf("\t\t Char [ Decimal Hexadecimal]\n\n");
	for(int elems = 1; elems < get_size_array(arr); ++elems) {
			  printf("%c:[ %d  0x%x]\t",arr[elems - 1],(int)arr[elems - 1],arr[elems -1]);
	 	

		  if(elems > 0 && elems % 4 == 0) printf("\n");
	}
	printf("\n");
}
