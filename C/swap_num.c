// Swap number using XOR 
//     gcc -g  -Wall swap_num.c -DXOR -o ~/EXEC/SWAP_XOR
// Swap number using Pointers
//     gcc -g -Wall swap_num.c -DPTR -o ~/EXE/SWAP_PTR
//

#include <stdio.h>
#include <stdlib.h>

int main() {
	int a,b;
#if defined(XOR)
	printf("Enter Two Number: (x y): ");
	scanf("%d\t%d",&a,&b);
	printf("Values before swaping: %d %d\n",a,b);
	
	a = a^b;
	b = a^b;
	a = a^b;

#elif defined(PTR)
	int *ptr1 = NULL,*ptr2 = NULL,*tmp = NULL;
	
	ptr1 = malloc(sizeof(int));
	ptr2 = malloc(sizeof(int));	
	tmp = malloc(sizeof(int));
	
	printf("Enter two number(x y): ");
	scanf("%d\t%d",&a,&b);
	
	printf("Values before swaping: %d %d\n",a,b);
	ptr1 = &a;
	ptr2 = &b;
	*tmp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = *tmp;
#endif
	printf("Values after swaping: %d %d\n",a,b);
}
