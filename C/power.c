#include <stdio.h>

int power(int base, int n) {
	int p = 1;
	for(int i = 1; i <= n; ++i)
		p = p * base;
	return p;
}

int main() {
	for (int i = 1; i < 10; ++) {
	printf("%d = %d",i, power(2,i));
	}	
}
