/* Simple TCP Client
 *
 * cc -g -Wall -w TCP_Client.c -o TCP_Client
 *
 *
 *
 */

#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include <netdb.h>

#define MAX 80
#define PORT 8080
#define SA struct sockaddr

void communication(int sockfd)
{
	char buffer[MAX] = " ";
	int n;
	for(;;) {
		bzero(buffer,sizeof(buffer));
		n = 0;

		printf("Enter message: ");
		while((buffer[n++] = getchar ()) != '\n');
		write(sockfd, buffer, sizeof(buffer));
		bzero(buffer,sizeof(buffer));
		read(sockfd, buffer, sizeof(buffer));
		printf("From Server: %s", buffer);
		if((strncmp(buffer, "exit", 4)) == 0) {
			printf("Client Exit...\n");
			break;
		}
	}
}

int main(int argc,char *argv[])
{
	int sockfd, connfd;
	struct sockaddr_in server_address, cli;

	// Socket Create and verification
	sockfd = socket(AF_INET, SOCK_STREAM,0);
	if(sockfd == -1) {
		printf("Socket creation failed \n");
		exit(0);
	}
	else 
		printf("Socket succesfully created\n");

	// Assign PORT, IP
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr("192.168.2.10");
	server_address.sin_port = htons(PORT);

	//Connect the client socket to server 
	if(connect(sockfd, (struct sockaddr* )&server_address, 
				sizeof(server_address)) != 0) {
		printf("Connection with the server failed\n");
		exit(0);
	}
	else
		printf("Connected to the server..\n");

	communication(sockfd);

	// Close the socket
	 close(sockfd);
	return 0;
}
