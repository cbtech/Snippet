/* Simple TCP Server 
 *
 * cc -g -Wall -w TCP_Server.c -o TCP_Server
 *
 */

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>

#define PORT 8080

long unsigned int strlen(const char* str)
{
	int i = 0;
	while(*str++ != '\0')	i++;
	return i;
}

void communicate(int connfd)
{
	char buffer[1024] = {0};
	int n;
	for(;;) {
		bzero(buffer,sizeof(buffer));
	}
}

int main(int argc,char **argv)
{
	int socket_fd, new_socket, valread,connfd;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char *hello = "Hello from Server\n";

/*	 Creating socket file descriptor 
		 int sockfd = scoket(domain , type, protocol)
		 DOMAIN: integer, specifies communication domain.
		 		AF_LOCAL = POSIX std for communication between 
		 							processes on the same host
		 		AF_INET  → IPV4 
		 		AF_INET6 → IPV6
 		 TYPE: communication type
				SOCK_STREAM:  TCP (reliable, connection oriented)
											UDP (unreliable, connectionless)
*/

	if ((socket_fd = socket(AF_INET, SOCK_STREAM,0)) == 0 )
	{
		fprintf(stderr, "Socket creation failed");
		exit(EXIT_FAILURE);
	}

	// Helps manipulatings options for the socket reffered by the file descriptor(sockfd)
	// Optional but prevents error such as "Adress already in use" 
	if(setsockopt(socket_fd,SOL_SOCKET,SO_REUSEADDR |
				SO_REUSEPORT,&opt,sizeof(int)))
	{
			fprintf(stderr,"Adress already in use\n");
			exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	// Bind Name to a socket
	if (bind(socket_fd, (struct sockaddr *)&address, sizeof(address)) <0 )
	{
		fprintf(stderr, "Bind error");
		exit(EXIT_FAILURE);
	}

	if (listen(socket_fd,5) < 0)
	{
		fprintf(stderr,"Listening failed\n");
		exit(EXIT_FAILURE);
	}
	
	if ((new_socket = accept(socket_fd, (struct sockaddr *)&address,
					(socklen_t*)&addrlen))<0)
	{
		fprintf(stderr,"Accept\n");
		exit(EXIT_FAILURE);
	}

	communicate(connfd);
	send(new_socket, hello, strlen(hello), 0);
	fprintf(stderr, "Msg sent\n");
	close(socket_fd);
	return 0;


}
