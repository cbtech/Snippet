 // Exo 1 tee Command Michael Kerrisk Chap 4 p 131
 // gcc -g -Wall Exo1_tee.c -o ~/EXEC/TEE
 //
 // tee reads std input and writes to a std output
 // and to a file provided on the command line arg.
 // by default, tee overwrites any existing file with the given name.
 // 
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 16 * 1024

int main(int argc,char *argv[]) {
    int bytes_read;
    size_t bytes_written;
    char buffer[BUFFER_SIZE];
    memset(buffer,0,BUFFER_SIZE);
    char *file_mode = malloc(sizeof(char));
    file_mode = "w";
    FILE *file;
    
    // Usage 
    if(argc <= 2) {
        printf("EXO1_TEE -a file\n");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1],"-a"))
        file_mode = "a+";

    file = fopen(argv[2],file_mode);

    while(1) {
        // 1. Read standard input 
        bytes_read = read(STDIN_FILENO,buffer,BUFFER_SIZE);
       
        // 2. Write to stdout(screen)
        if(write(STDOUT_FILENO,buffer,bytes_read) != bytes_read )
                fprintf(stderr,"Error writing to STDOUT\n");
    
        // 3. Write to file 
        if(bytes_written = fwrite(&buffer, BUFFER_SIZE ,1,file) == 0) 
            fprintf(stderr,"Error writing file\n");

        printf("Read: %d, write: %ld\n",bytes_read,bytes_written);
        break;
        // 4. Ok all bytes is written we can quit
        if (bytes_written == bytes_read ) 
            break;
        
    }

    fclose(file);
}
