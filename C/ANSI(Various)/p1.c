//   C examples from ANSI C 
//	tcc -g -Wall chap1.c -DGET_CHAR -o GET_CHAR
//	tcc -g -Wall chap1.c -DCOUNT_CHAR -o COUNT_CHAR
//	tcc -g -Wall chap1.c -DLINE_COUNT -o LINE_COUNT

#include <stdio.h>

void get_char() {
	int c;

	while(( c = getchar()) != EOF) {
		putchar(c);
	}
}

void count_char() {
	double nc;
	printf("Count char in string. Press CTRL+D to show the result.\n\n");
	for(nc = 0; getchar() != EOF; ++nc)
		;
	printf("%.0f\n",nc);
}

void count_line() {
	int c,nl;

	nl = 0;
	printf("Count line in string. Press CTRL+D to show the result.\n\n");

	while( (c = getchar()) != EOF)
		if (c == '\n')
			++nl;
	printf("%d\n",nl);
}

#define IN 1   // Inside word
#define OUT 0  // Outside word
	
void count_words() {
	int c, nl, nw, nc, state;

	state = OUT;
	nl = nw = nc = 0;

	printf("Count word in string. Press CTRL+D to show the result.\n\n");
	while((c = getchar()) != EOF) {
		++nc;
		if(c == '\n')
			++nl;
		if (c == ' ' || c == '\n' || c == '\t')
			state = OUT;
		else if (state == OUT) {
			state = IN;
			++nw;
		}
	}
	printf("Lines: %d\t Words: %d \tChars: %d\n", nl, nw, nc);

}

int main(int argc,char **argv[])
{
	#if defined(GET_CHAR)
		get_char();
	#elif defined(COUNT_CHAR)
		count_char();
	#elif defined(COUNT_LINE)
		count_line();
	#elif defined(COUNT_WORDS)
		count_words();
  	#endif

	return 0;
}
