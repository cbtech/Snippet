/*     C examples from ANSI C 
 *  cc -g -Wall p3.c -DPOWER -o POWER
 */

#include <stdio.h>

int power(int base,int n) {
	int i, p;
	 p = 1;
	 for(i = 1; i <= n;++i)
		 p = p * base;
	 return p;
}

/*********************** GETLINE *****************************/
#define MAXLINE 1000 // maximun inputline length
int get_line(char s[],int lim)
{
	int c,i;

	for(i = 0; i < lim-1 && (c=getchar()) !=EOF && c !='\n'; ++i)
		s[i] = c;
	if(c == '\n') {
		s[i] = c;
		i++;
	}
	s[i]='\0';
	return i;
}

void copy(char to[], char from[])
{// Copy 'from' into 'to'; Assume to is big enough
 	int i;

	i = 0;
	while((to[i] = from[i]) != '\0')
		++i;
}

int main(int argc,char *argv[])
{
	#if defined(POWER)
		const int EXPONENT = 2;
		for (int base  = 0; base < 10; ++base)
			printf("Base %d exp(2) =  %d \n",base, power(base,EXPONENT));
	#elif defined(GETLINE)
		int len;		// current line length
		int max;		// max length seen so far
		char line[MAXLINE];	// current input line
		char longest[MAXLINE];	// longest line saved here

		max = 0;
		while((len = get_line(line,MAXLINE)) > 0)
			if(len > max) {
				max = len;
				copy(longest, line);
			}
		if(max > 0) 		// There was a line 
			printf("Longest Line :  %s", longest);
			
	#endif

	return 0;
}
