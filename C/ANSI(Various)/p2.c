/* Print Fahreinheit-celsius table 
	for fahr = 0, 20, ... , 300 

	cc -g -Wall p2.c -DCELSIUS -o CELSIUS
	cc -g -Wall p2.c -DCELSIUS_LOOP -o CELSIUS_LOOP

*/

#include <stdio.h>
void show_celsius()
{
	float fahr, celsius;
	float lower, upper, step;

	lower = 0;		/* Lower limit of temperature scale */
	upper = 300;	/* upper limit */
	step = 20;			/* step size */

	fahr = lower;
	while (fahr <= upper) {
	//	celsius = 5 * (fahr - 32) / 9; INT VERSION( not FLOAT)
		celsius =(5.0/9.0) *(fahr-32.0);
		printf("%3.0f \t%6.1f\n", fahr, celsius);
		fahr = fahr + step;
	}
}

void celsius_loop()
{
	/* print fahreinheit - celsius */
	for (int fahr = 0; fahr <= 300; fahr = fahr + 20)
		printf("%3d %6.1f\n",fahr, (5.0/9.0) * (fahr -32));
}

void array() {
/* Count digits, white space, others */
	int c, i, nwhite, nother;
	int ndigit[10];

	nwhite  = nother = 0;
	for(i = 0; i < 10; ++i)
	{
		ndigit[i] = 0;
	}
	while((c = getchar()) != EOF)
	  if( c >= '0' && c <= '9')
	  ++ndigit[c-'0'];
	else if (c == ' ' || c == '\n' || c == '\t')
  	  ++nwhite;
	else ++nother;
	
	printf("Digits =");
	for(i = 0; i < 10; ++i)
	  printf(" %d ", ndigit[i]);
	printf(", Whitespace = %d, other = %d\n",nwhite,nother);
}

int main ()
{
	#if defined(CELSIUS)
		show_celsius();
	#elif defined(CELSIUS_LOOP)
		celsius_loop();
	#elif defined(ARRAY)
		array();
	#endif
	return 0;
}
