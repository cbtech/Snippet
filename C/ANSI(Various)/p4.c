#include <stdio.h>
#include <string.h>

int atoi(char s[]) {
	int i,n;

	n = 0;
	for(i = 0;s[i] >= '0' && s[i] <= '9'; ++i)
		n = 10 *n + (s[i] -'0');

	return n;
}

int lower(int c) {
	if(c >= 'A' && c <= 'Z')
		return c + 'a' - 'A';
	else
		return c;
}

double sqrt(double d) {
	double tmp = d * d;
	return tmp;
}

unsigned long int next =1;
int rand(void) {
	// Return Pseudo-random integer on 0.. 32767
	next = next * 1103515245 +12345;
	return (unsigned int)(next / 65536) % 32768;
}

void srand(unsigned int seed) {
	next = seed;
}

int main(int argc,char*argv[]) {
#if defined(ATOI)
	int val;
	char *str = "888555333";
	val = atoi(str);
	printf("Atoi: %d\n",val);

#elif defined(LOWER)
	char letter= 'D';
	printf("%c",lower(letter));

#elif defined(SQRT)
	int val = 6;
	int squared = sqrt((double)val);

	printf("Squared roots of : %d is: %d",val,squared);
#elif defined(RAND)
	for(int i =0; i < 10;i++){
	int rand_val = rand() %100;
	printf("Random value %d: %d\n",i,rand_val);
	}
#endif

}


