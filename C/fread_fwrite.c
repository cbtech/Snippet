#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct {
	int start_time;
	char *state;
} pomodoro;

const char *file = "/tmp/wtimer";

void set_value(pomodoro *p)
{
	p->start_time = time(NULL);
	p->state="WORK";
}

void read_func(pomodoro *p)
{	
	FILE *fp;
	int read_value = 0;
	int read_struct_value = 0; // start_time
	char *str = " ";
	
	if((fp = fopen(file,"rb+")) == NULL) {
		printf("Cannot open file\n");
		exit(1);
	}
	// READ X
	fread(&read_value,sizeof(int),1,fp);
	// READ STRUCT INT
	fread(&read_struct_value,sizeof(int),1,fp);
	// READ STRUCT CHAR (p->state)
	fread(&str,sizeof(char),1,fp);

	printf("[READ] x: %d \tstart_time: %d \tState: %s \n",
			read_value,read_struct_value,str);
	fclose(fp);
}

void write_func(pomodoro *p)
{
	FILE *fp;
	int x=1234;
	
	if((fp=fopen(file,"wb+")) == NULL) {
		printf("Cannot open file.\n");
		exit(1);
	}
	set_value(p);
	printf("Writing start_time: %d \t state: %s \n",p->start_time,p->state);
	// PASSING SIMPLE INT
	fwrite(&x,sizeof(int),1,fp);
	// PASSING STRUCT INT
	fwrite(&p->start_time,sizeof(int), 1, fp);
	// PASSING STRUCT CHAR
	fwrite(&p->state,sizeof(char), 1, fp);
	fclose(fp);
}

int main(int argc,char **argv)
{
	pomodoro p;


	#if defined(WRITE)
		write_func(&p);
	#elif defined(READ)
		read_func(&p);
	#endif

	
}


