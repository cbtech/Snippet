// DaftS Scraper using curl
// tcc  -g -Wall -w daftscrape.c -lcurl -o DAFTSCRAPE
//

#include <curl/curl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <regex.h>

typedef struct {
	char* buffer;
	size_t size;
} memory;

size_t grow_buffer(void *content,size_t size,size_t nb_elems,void *ctx)
{
	size_t real_size = size * nb_elems;
	memory *mem = (memory*) ctx;
	char *ptr = realloc(mem->buffer, mem->size + real_size);
	if (!ptr) {
		fprintf(stderr,"Not enough memory(Realloc failed)\n");
		return 0;
	}
	mem->buffer = ptr;
	memcpy(&(mem->buffer[mem->size]),content,+ real_size);
	mem->size += real_size;
	return real_size;

}

int main(int argc,char **argv)
{
	// GET FIRST ARG
	//const char *url = argv[1];
	memory *mem = malloc(sizeof(memory));

	const char *url = "https://daftsex.com/watch/-45836141_456239619";
	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();

	if(curl) {
		// READ THE CONTENT TO TTY
		//READ THE CONTENT AND EXPORT IT WHEREVER YOU WANT
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,grow_buffer);
		curl_easy_setopt(curl,CURLOPT_WRITEDATA,mem);
		curl_easy_setopt(curl,CURLOPT_URL,url);
		res = curl_easy_perform(curl);

		// PRINT BUFFER CONTENT
		printf("BUFFER: %c\n",mem->buffer[0]);
	}

	return 0;
}
