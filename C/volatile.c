// Volatile keyword in C
// 
// volatile keyword is intended to prevent the compiler from applying
// any optimizations on objects that can change in ways that cannot 
// be determined by the compiler
//
// 1. Global var modified by an interrupt service routine outside the scope 
// 	Global vars can represent a data port (global pointer referred 
// 	as memory mapped IO) which will updated dynamically.
// 	The code reading data port must be declared as volatile 
// 	in order to fetch latest data available at the port.
//	Failing to declare variable as volatile.the compiler will
//	optimize the code in such way that it will read the port
//	only once and keep using the same value.
//
// 2. There are multiple ways for threads communication, vizualisation,
// 	message passing, shared memory, mail boxes, etc. A global variable 
// 	is weak form of shared memory. When two threads sharing information
// 	via global variable, they need to be qualified with volatile.
// 	Since threads run asynchronously, any global variable and can place 
// 	them in temporary variable of current thread context.
// 	To nullify the effect of compiler optimizations, such global variables
// 	need to be qualified as volatile
//
// If we do not use volatile qualifier, the following problems may arise
//   1. Code may not work as expected when optimization is turned on
//   2. Code may not work as expected when interrupts are enabled and used.
//
// WITHOUT_OPT: 5.9k
//
// gcc -g -Wall -w -DWITH_OPT -o Volatile -save-temps
//   with -save-temps gcc generates 3 output files
//   	1. preprocessed code (extention .i)
//	2. Assembly code (extention .s)
//	3. Object code (extention .o)
//
// WITH OPT: 8.6k
// 	add -O3 to gcc command line
//
// volatile WITH_OPT: 8.4K

#include <stdio.h>

int main(int argc,char *argv[])
{
	#if defined(WITHOUT_OPT)
		printf(" ---- [ Code without optimization option ] ---- \n");
		
		const int local = 10;
		int *ptr =(int *)&local;
		printf("Initial value of local: %d\n", local);
		
		*ptr = 100;
		printf("Modified value of local: %d \n", local);

	#elif defined(WITH_OPT)
		printf(" ---- [ Code with optimization option ] ---- \n");

		const volatile int local = 10;
		int *ptr = (int *)&local;
		printf("Initial value of local: %d\n",local);

		*ptr = 100;

		printf("Modified value of local: %d\n", local);
		
		return 0;
	#endif
	
}


