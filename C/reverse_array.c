#include <stdio.h>
#include <stdlib.h>

#define PREV(x) x - 1

int main() {
	int nb_elems = 0, idx,n;
	int *arr;
	arr = malloc(nb_elems * sizeof(int));

	
	printf("Enter the number of elements: ");
	scanf("%d",&nb_elems);
	for(idx = 0; idx < nb_elems; idx++){
		printf("Enter number %d:  ",idx);
		scanf("%d",&arr[idx]);
	}
	// SORT
	for(n = 1 ; n <= nb_elems; n++) {
		arr[n] = arr[n] ^ arr[PREV(n)];
		arr[PREV(n)] = arr[n] ^ arr[PREV(n)];
		arr[n] = arr[n] ^ arr[PREV(n)];
	}
	
	// SHOW RESULT
	for (n = 0; n < nb_elems; n++) {
		printf("%d Value: %d\n",n,arr[n]);
	}

	return 0;
}
