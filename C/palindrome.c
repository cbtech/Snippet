#include <stdio.h>
#include <string.h>

#define STR_LENGTH 256

int main() {
	char str[STR_LENGTH];
	int i,j;
	printf("Enter the string: ");
	fgets(str,STR_LENGTH,stdin);
	for(i = 0,j=strlen(str)-1;i < strlen(str) /2; 
			i++,j--) {
		if(str[i] != str[j - 1]) {
			printf("Not a palindrome\n");
			break;
		}
	}
	if(i==strlen(str) /2 || i == j) {
		printf("PALINDROME\n");
	}

	return 0;
}
