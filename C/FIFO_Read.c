/* 
 *
 * FIFO communication between process
 *
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h> // read

int main(int argc,char *argv[])
{
	int arr[5];
	char *fifo_file="/tmp/fifo";

	int fd = open(fifo_file,O_RDONLY);

	if (fd == -1) return 1;

	if(read(fd,arr, sizeof(int) * 5) == -1)
		fprintf("Reading file is not possible\n";

	close(fd);

	int sum = 0;
	for(int i = 0; i < 5; i++) {
		printf("Received %d\n",arr[i]);
		sum += arr[i];
	}

	printf("Result is %d\n",sum);
	
	

	return 0;
}

