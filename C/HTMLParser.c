#include <stdio.h>

// Func to parse HTML code
void parser(char *S)
{
	// Store the length of input string
	int n = strlen(S);
	int start = 0, end = 0;
	
	// Traverse the string 
	for(int i = 0; i < n; i++) {
	// if S[I] is '>', update
	// start to i+1 and break	
	// <h1    > ← Ok we can START
		if (S[i] == '>') {
			start = i+1;
			break	;
		}
	}
	
	// Remove the blank space 
	while (S[start] == ' ') {
		start++;
	}

	for(int i = start; i < n; i++) {
	  // If S[i] is < , update
	  // Ok We can STOP → <   /h1>
	  // end to i-1 and break
	  if (S[i] =='<') {
		  end = i - 1;
		  break;
	  }
	}
	//Print the character in the range
	for(int j = start; j <= end; j++)
	{
		printf("%c",S[j]);
	}

	printf("\n");
}

int main(int argc,char *argv[])
{
	char input1[] = "<h1>This is a statement</h1>";
	char input2[] = "<h1>                  This is a statement with some space </h1>";
	char input3[] = "<p> This is a statement with some @#$.,/ specials characters</p>    ";

	parser(input1);
	parser(input2);
	parser(input3);
	
}
