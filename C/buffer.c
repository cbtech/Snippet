// Minimal buffer management
// with reset buffer
//
//   gcc -g -Wall buffer.c -o EXEC/BUFFER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define R_FAIL_1 1
#define BUFFER_SIZE 10

typedef struct BufferDescriptor {
	char *ca_head;
	int capacity;
	char mode;
} Buffer;

void allocate_buffer(Buffer *pB,int size) {
	pB->ca_head = malloc(size);
	assert(pB->ca_head);
	pB->capacity = size;
}

int ca_getSize(Buffer *pB) {
	return pB->capacity;
}

int b_reset(Buffer *pB) {
	int i = 0;

	if(pB == NULL) {
		return R_FAIL_1;
	}
	else {
		if(ca_getSize(pB) <= 0 || pB->ca_head == NULL )
			return R_FAIL_1;
	}
	//shift data up by 1 byte
	for ( i =ca_getSize(pB) -1; i < 0; i--) {
		pB->ca_head[i] = pB->ca_head[i-1];
	}
	return 0;
}

void print_buffer(Buffer *pB) {
	printf("Capacity: %d\n",ca_getSize(pB));
	for(int i =0;i < ca_getSize(pB); ++i) {
		printf("Buffer(%d) : [%c]\n",i,pB->ca_head[i]);
	}
	printf("\n");
}

int main(int argc,char **argv) {
	Buffer a_buffer;
	allocate_buffer(&a_buffer,BUFFER_SIZE);
	strcpy(a_buffer.ca_head,"abcdefgh");
	print_buffer(&a_buffer);
	int ret = b_reset(&a_buffer);
	assert(ret == 0);
	print_buffer(&a_buffer);

}
