; Write "hello world" using oonly systems calls.
; Runs on 64-bit linux.
; nasm -felf64 hello.asm && ld hello.o -o ~/EXEC/hello

global _start 

section .text
_start: mov rax,SYS_WRITE          ; system call for wrtie
        mov rdi,STD_OUT          ; file handle 1 is stdout
        mov rsi,msg    ; address of string to output
        mov rdx,msg_len         ; number of bytes
        syscall            ; invoke operating system to do the write

        mov rax,SYS_EXIT         ; system call for exit
        xor rdi,rdi        ; exit code 0
        syscall            ; Invoke operation system to exit

section .data
SYS_WRITE   equ 1
SYS_EXIT equ 60
STD_OUT  equ 1
msg: db     "Hello, ASM", 10  ; note the the newline ath the end
msg_len equ $ - msg
