; User input using assembly x64
; nasm -f elf64 input.asm 
; ld -o input input.o
;

section .bss
    occurence_nb resb 2   ; Reserve 2 bytes for occurence_nb
     
global _start
section .text
_start:
    ; Show msg
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    mov rsi, msg
    mov rdx, msg_len
    syscall 
    
    ; Read input
    mov rax, SYS_READ           ; Read input
    mov rdi, STD_IN
    mov rsi, occurence_nb
    mov rdx, 2                  ; Size of reading
    syscall
    
    ; Show input
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    mov rsi, occurence_nb
    mov rdx, 2
    syscall

    ; Exit
    mov rax,SYS_EXIT
    xor rdi,rdi
    syscall

section .data
    SYS_WRITE equ 1
    SYS_READ equ 0
    SYS_EXIT equ 60
    STD_IN equ 0
    STD_OUT equ 1

    msg: db "Enter the number of occurences: "
    msg_len equ $ - msg
