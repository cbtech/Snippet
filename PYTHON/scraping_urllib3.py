#!/usr/bin/python3

# [RESOURCES]
# https://zetcode.com/python/urllib3/
# https://urllib3.readthedocs.io/

import urllib3
import json
import certifi

# PoolManager is needed to make requests.
# Certificates is for https website
http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())

# To make a request use request(). 
r = http.request('GET','http://httpbin.org/robots.txt')
r.data

print(r.data.decode('utf-8'))

# JSON FORMAT

## REQUEST DATA HEADERS
r_json = http.request('GET','http://httpbin.org/headers',
        headers={'X-something' : 'value' },
        )

print(json.loads(r_json.data.decode('utf-8'))['headers'])

#print(request.data.decode('utf-8'))

# SHOW ALL VALUE IN REQUEST.HEADERS
print("HEADERS: ", r.headers)
for key,value in r.headers.items():
    print("KEY: ",key,"\tValue :", value , "\n")

## QUERY PARAMETERS
#  for GET / HEAD / DELETE requests, you can simply pass the arg
#  as a dictionary in the field argument to request()

r_json = http.request('GET', 'http://httpbin.org/get',
        fields={'arg':'value'})

print(json.loads(r_json.data.decode('utf-8'))['args'])

## for POST / PUT requests, you need to manually encode
#  query parameters in the url

## HTML FORM
from urllib.parse import urlencode
## GET FORM FIELDS USING WEBDEVTOOL
url = 'http://httpbin.org/post'
r = http.request('POST',url,
        fields= {'custname': 'Jack Lemaitre',
            'custemail': 'jackLemaitre@webmail.com',
            'custtel': '999666777',
            'delivery':'12:15',
            'size':'small',
            'topping':'bacon'
})

print("POST FORM HTTPBIN")
print(r.data.decode('utf-8'))

## JSON FORM REQUEST
data = {'custname': 'Jack Lemaitre',
        'custemail': 'jackLemaitre@webmail.com',
        'custtel': '999666777',
        'delivery':'12:15',
        'size':'small',
        'topping':'bacon'}

encoded_data = json.dumps(data).encode('utf-8')
r = http.request('POST','http://httpbin.org/post',
        body=encoded_data,
        headers={'Content-Type':'application/json'})

json_value = json.loads(r.data.decode('utf-8'))['json']
print("--- | JSON FORM REQUEST | ---\n")
print(json_value)

# SEND FILES AND BINARY DATA
with open('file.txt') as fp:
    file_data = fp.read()

r = http.request('POST', 'http://httpbin.org/post',
        fields={
            'filefield': ('example.txt', file_data),
            }
        )
response = json.loads(r.data.decode('utf-8'))['files']
print(response)

## TIMEOUTS
http.request('GET','http://httpbin.org/delay/3',timeout=4.0)
## OR
http.request('GET','http://httpbin.org/delay/3',
        timeout=urllib3.Timeout(connect=1.0))

## RETRYING REQUESTS
http.requests('GET','http://httpbin.org/ip', retries= 10)

# Disable All retries
http.requests('GET','http://httpbin.org/ip', retries= False)
