#!/usr/bin/python3

n = 7

# NON RECURSIVE METHOD

fact = 1
while n > 0:
    fact = fact * n
    n -=1

print (fact)

# RECURSIVE METHOD 

def factorial(n):
    if n < 1:
        return 1
    else:
        number = n * factorial( n - 1 )
        return number

print (factorial(7))

# FIBONACCI
def fibonacci(n):
    a,b = 0,1
    a, b = b, a + b
    return a

print(fibonacci(2))

def fibonacci2(n):
    if n <= 1:
       return n
    else:
       return (fibonacci2(n-1) + fibonacci2(n-2))
