#!/usr/bin/python3


import urllib3
import json
import os
from base64 import b64decode
import sys
import certifi
import re           # for regular expression
from pudb import set_trace

http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())

# Light Green Car 240/360/480/1080/720/
# GET VIDEO ID (-2000072782_95072782)
# 2012 X-Games 18 motocross best trick 720/480/360/240 28:50min
url = sys.argv[1]
print("URL: " , url)
#url='https://daftsex.com/watch/-45836141_456239619'
#url='https://daftsex.com/watch/-159995111_456239039'

pattern_id = re.search(r'https?://(?:www\.)?daftsex\.com/watch/(?P<id1>-\d+)_(?P<id2>\d+)',url)
print("ID1: ",pattern_id.group(1))
print("ID2: ",pattern_id.group(2))

# GET VIDEO ID USING SLICE
id = url[26:46]

request = http.request('GET',url)

# FIRST REQUEST - GET PAGE SOURCE
content = request.headers["Content-Type"]

# GET WEBPAGE CONTENT
webpage = request.data.decode('utf-8')

# WRITE FIRST PAGE SCRAPING TO FILE
file = "scraping_DS1.md"
with open(file ,'a') as file_obj:
    file_obj.write(webpage)

# GET TITLE / DURATION / PLAYER_HASH / PLAYER_COLOR
title = re.findall(r'<title>(.*?)</title>',str(webpage))
print("Title: ", title[0])

duration = re.findall(r'Duration: ((?:[0-9]{2}:)[0-9]{2})',str(webpage))
print("Duration: " , duration[0],"\n")

player_hash = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*hash:\\s*"([0-9a-zA-Z_\\-]+)"[\\s\\S]*}',str(webpage))
print("Hash:", player_hash[0])

player_color = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*color:\\s*"([0-9a-z]+)"[\\s\\S]*}',str(webpage))
print("Color:" ,player_color[0])

## SECOND PART 
url2 = 'https://daxab.com/player/' + player_hash[0] +'?color=' + player_color[0]
print("URL2: ", url2)

request2 = http.request('GET',url2,headers={'Referer': url})
webpage2 = request2.data.decode('utf-8')

## WRITE SECOND PAGE SCRAPPING
file2 = "scraping_DS2.md"

if(os.path.exists(file2)):
    os.remove("scraping_DS2.md")

with open(file2,'a') as file2_obj:
    file2_obj.write(webpage2)

# GET VIDEO PARAMS - GET QUALITY AND VIDEO ID
video_params=re.findall(r'window\.globParams\s*=\s*({[\S\s]+})\s*;\s*<\/script>',str(webpage2))
print("\n---- [ VIDEO PARAMETERS] ----\n")

server_matches = re.search(r'server:\s"(?P<server>[^"]*)',str(video_params))
#print(type(server_matches.group('server')))

# GET SERVER INFORMATION(DECYPT SERVER URL)
server_name = b64decode(server_matches.groups('server')[0][::-1]).decode('utf-8')
server = 'https://%s' % server_name
print("SERVER_NAME:",server)

# FORCE QUALITY
ext='mp4'
#quality='720'
# RETURN LIST OF ALL QUALITY 
quality = re.findall(r'mp4_(?P<quality>\d+)',str(video_params),re.MULTILINE)
print("QUALITY", quality)
for i in range(len(quality)):
    if quality == '720':
        print("720P selected\n")
        selected_quality = '720'
    else:
        selected_quality = quality[-1]
        print(selected_quality, "Selected\n")

hash_file = re.search(r''+ ext +'_'+ selected_quality +'":\"'+ selected_quality +'.(?P<hash>[^"]*)',str(video_params))
print("HASH: ", hash_file.group('hash'))

f_url = f'{server}/videos/{pattern_id.group(1)}/{pattern_id.group(2)}/{selected_quality}.{ext}'
print("FINAL URL: ", f_url)

# CHANGE ALL SPACE WITH \ 
f_title = title[0].replace(" ", "\ ")
print("FORMATED TITLE: ", f_title)

#aria_cmd = ('aria2c -x16 -s16 -k10M ' + f_url + ' -o ' + title[0] + '.' + ext)
aria_cmd = f'aria2c -x16 -s16 -k10M -o {f_title}-{selected_quality}.{ext} {f_url}'
print("ARIA_CMD: ",aria_cmd)

os.system(aria_cmd)
