#!/usr/bin/python3

import socket 

hostname = socket.gethostname()
local_ip = socket.gethostbyname(hostname)

print(f"Hostname: {hostname}")
print(f"LocalIP:  {local_ip}")

