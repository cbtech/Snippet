#!/usr/bin/python3

import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import  MIMEBase
from email.mime.multipart import MIMEMultipart

#server = smtplib.SMTP('smtp.free.fr',25)
server = smtplib.SMTP_SSL('smtp.free.fr',465)
server.ehlo()

username = 'yourusername'
password = 'yourpassword' 
server.login(username,password)

msg = MIMEMultipart()
msg['From'] =  'NeuralNine'
msg['to'] = 'captainchrisprod@free.fr'
msg['Subject'] = 'Just a TEST'

with open ('text.txt','r') as f:
    message = f.read()
 
msg.attach(MIMEText(message, 'plain'))

filename = 'html_code_650.jpg'
attachment = open(filename ,'rb')

p = MIMEBase('application','octet-stream')
p.set_payload(attachment.read())

encoders.encode_base64(p)
p.add_header('Content-Disposition',f'attachment,filename={filename}')
msg.attach(p)

text = msg.as_string()
server.sendmail('christophebourgeois2@free.fr','captainchrisprod@free.fr', text)
server.quit()




