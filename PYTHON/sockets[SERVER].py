#!/usr/bin/python3

''' 
Socket is basically a end point of communication channel
'''

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1',55555))
s.listen()

while True:
    client,adress = s.accept()
    print("Connectedtp {}".format(adress))
    client.send("You are connected!".encode())
    client.close()

