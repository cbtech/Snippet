#!/usr/bin/python3

import sqlite3

class Person:
    def __init__(self,id_number,first,last,age):
        self.id_number = id_number
        self.first = first
        self.last = last 
        self.age = age
        self.connection = sqlite3.connect('mydata.db')
        self.cursor = self.connection.cursor()

    def load_person(self,id_number):
        cursor.execute("""
        SELECT * FROM persons
        WHERE id = {}
        """).format(id_number)

        results = cursor.fetchone()
        self.id_number = id_number
        self.first = result[1]
        self.last = result[2]
        self.age = result[3]

    def insert_person(self):
        self.cursor.execute("""
INSERT INTO persons VALUES({}, '{}','{}',{})
        """.format(self.id_number,self.first,self.last, self.age))
        self.connection.commit()
        self.connection.close()

""" CREATE TABLE WITH COMMAND LINE SQL
    sqlite3 mydata.db
    create table persons(id_number INTEGER, first TEXT, last TEXT,age INTEGER);
    insert into persons values('1','Jack','Lemaitre','39');
    insert into persons values('1','Michel','Tremauret','36');
    insert into persons values('1','Yohan','Melanche','7');
    insert into persons values('1','Stupid','Guy','5');
    insert into persons values('1','Idiot','Yanis','2');

""" 


p1 = Person (7,"Alex","Robbins",30)
p1.insert_person()

connection = sqlite3.connect('mydata.db')
cursor = connection.cursor()

cursor.execute("SELECT * FROM persons")
results = cursor.fetchall()
print(results)

connection.close()

