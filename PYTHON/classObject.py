#!/usr/bin/python3

class Person:
    amount = 0

    def __init__(self,name,age,height):
        self.name = name 
        self.age = age
        self.height = height
        Person.amount += 1

    def __del__(self):
        Person.amount -= 1
        print("Object deleted")

    # USE PRINT(PERSON) 
    def __str__(self):
        return "Name: {}, Age: {}, Height: {}".format(self.name,self.age,self.height)

v = Person("Jack",38,170)
w = Person("Michel",33,170)
x = Person("Yohan",6, 90) 

print(x)

print(x.amount)
# DELETE OBJECT
del(x)

input("End. Press a key to quit")
