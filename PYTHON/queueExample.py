#!/usr/bin/python3

''' Third Example :: PriorityQueue'''
import queue

q = queue.PriorityQueue()

q.put((1,"Hello World"))
q.put((20,999))
q.put((10,True))
q.put((15,False))

while not q.empty():
    print(q.get())



'''Second Example :: Last input first Output 
from queue import LifoQueue

q = LifoQueue()

numbers=[1,2,3,4,5,6,7,8,9,0]

for number in numbers:
    q.put(number)

print(q.get())
print(q.get())
print(q.get())
print(q.get())
'''

'''
First example:: Simple Queue 
from queue import Queue
q = Queue()
numbers = [ 10, 20, 30, 40, 50, 60, 70]

for number in numbers:
    q.put(number)

print(q.get())
print(q.get())
print(q.get())
print(q.get())
print(q.get())
'''
