#!/usr/bin/python3

# First example
# python3 dunders.py --main  

import sys

class SpecialString:
	def __init__(self, cont):
		self.cont = cont 

	def __truediv__(self,other):
		line = "=" * len(other.cont)
		return "\n".join([self.cont, \
			line, other.cont])

if sys.argv[1] == '--main':
	spam = SpecialString("spam")
	hello = SpecialString("Hello world!")
	print(spam / hello)

if sys.argv[1] == '--second':
	spam = SpecialString("spam")
	eggs = SpecialString("eggs")
	if(spam.__gt__('e')):
		print("Spam is greater then e\n")
